﻿using System;
using System.Collections.Generic;

namespace EducationPlatform.Models.EntityModels
{
    public partial class BotAuthorizedUsers
    {
        public int AuthorizedUserId { get; set; }
        public string UserId { get; set; }
        public string ChannelId { get; set; }
        public string ChannelUserId { get; set; }
        public string ConversationId { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
