﻿using System;
using System.Collections.Generic;

namespace EducationPlatform.Models.EntityModels
{
    public partial class MessangersBots
    {
        public int MessangersBotsId { get; set; }
        public string BotId { get; set; }
        public string ChannelId { get; set; }
        public string BotUrl { get; set; }
        public string ServiceUrl { get; set; }
    }
}
