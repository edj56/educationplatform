﻿using System;
using System.Collections.Generic;

namespace EducationPlatform.Models.EntityModels
{
    public partial class BotAuthorizationKeys
    {
        public int KeyId { get; set; }
        public string UserId { get; set; }
        public string AuthorizationKey { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
