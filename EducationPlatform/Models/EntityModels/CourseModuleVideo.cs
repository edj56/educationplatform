﻿using System;
using System.Collections.Generic;

namespace EducationPlatform.Models.EntityModels
{
    public partial class CourseModuleVideo
    {
        public int CourseModuleId { get; set; }
        public int VideoId { get; set; }

        public virtual CourseModule CourseModule { get; set; }
        public virtual Video Video { get; set; }
    }
}
