﻿using System;
using System.Collections.Generic;

namespace EducationPlatform.Models.EntityModels
{
    public partial class Video
    {
        public Video()
        {
            CourseModuleVideo = new HashSet<CourseModuleVideo>();
        }

        public int VideoId { get; set; }
        public string Link { get; set; }

        public virtual ICollection<CourseModuleVideo> CourseModuleVideo { get; set; }
    }
}
