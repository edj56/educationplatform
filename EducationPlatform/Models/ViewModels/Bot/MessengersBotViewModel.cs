﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Bot
{
    public class MessengersBotViewModel
    {
        public int MessangersBotsId { get; set; }
        public string BotId { get; set; }
        public string ChannelId { get; set; }
        public string BotUrl { get; set; }
        public string ServiceUrl { get; set; }

        public MessengersBotViewModel() { }

        public MessengersBotViewModel(MessengersBots model)
        {
            MessangersBotsId = model.MessengersBotsId;
            BotId = model.BotId;
            ChannelId = model.ChannelId;
            BotUrl = model.BotUrl;
            ServiceUrl = model.ServiceUrl;
        }
    }
}
