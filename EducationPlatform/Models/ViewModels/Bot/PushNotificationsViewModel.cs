﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Bot
{
    public class PushNotificationsViewModel
    {
        public Dictionary<string, string> Jsons { get; set; }
        public List<MessengersBots> MessangersBots { get; set; }
    }
}
