﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;

namespace EducationPlatform.Models.ViewModels.Bot
{
    public class BotAuthorizedUsersViewModel
    {
        public int AuthorizedUserId { get; set; }
        public string UserId { get; set; }
        public string ChannelId { get; set; }
        public string ChannelUserId { get; set; }
        public string ConversationId { get; set; }

        public UserViewModel User { get; set; }

        public BotAuthorizedUsersViewModel() { }

        public BotAuthorizedUsersViewModel(BotAuthorizedUsers model)
        {
            AuthorizedUserId = model.AuthorizedUserId;
            UserId = model.UserId;
            ChannelId = model.ChannelId;
            ChannelUserId = model.ChannelUserId;
            ConversationId = model.ConversationId;
            User = new UserViewModel(model.User);
        }
    }
}
