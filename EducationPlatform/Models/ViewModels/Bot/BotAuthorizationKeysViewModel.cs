﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Bot
{
    public class BotAuthorizationKeysViewModel
    {
        public int KeyId { get; set; }
        public string UserId { get; set; }
        public string AuthorizationKey { get; set; }

        public virtual UserViewModel User { get; set; }

        public BotAuthorizationKeysViewModel() { }

        public BotAuthorizationKeysViewModel(BotAuthorizationKeys model)
        {
            KeyId = model.KeyId;
            UserId = model.UserId;
            AuthorizationKey = model.AuthorizationKey;
            User = new UserViewModel(model.User);
        }
    }
}
