﻿namespace EducationPlatform.Models.ViewModels.Schedule
{
    public class ScheduleItemViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
    }
}
