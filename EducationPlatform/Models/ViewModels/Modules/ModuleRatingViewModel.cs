﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Modules
{
    public class ModuleRatingViewModel
    {
        public Course Course { get; set; }
        public Module Module { get; set; }
        public List<CourseModuleStudentsMark> Marks { get; set; }
        public int MinLabSum { get; internal set; }
        public int MaxLabSum { get; internal set; }
        public int MinTestSum { get; internal set; }
        public int MaxTestSum { get; internal set; }
    }
}
