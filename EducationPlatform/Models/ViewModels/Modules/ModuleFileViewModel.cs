﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Modules
{
    public class ModuleFileViewModel
    {
        public int ModuleId { get; set; }
        public int ModuleFileId { get; set; }
        public string FileUrl { get; set; }

        public ModuleFileViewModel() {}

        public ModuleFileViewModel(ModuleFile model) {
            ModuleId = model.ModuleId;
            ModuleFileId = model.ModuleFileId;
            FileUrl = model.FileUrl;
        }
    }
}
