﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Modules
{
    public class ModuleFileListViewModel
    {
        public List<ModuleFileViewModel> Files { get; set; }
        public ModuleViewModel Module { get; set; }
    }
}
