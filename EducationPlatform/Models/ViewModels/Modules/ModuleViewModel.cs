﻿using EducationPlatform.Models.EntityModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Modules
{
    public class ModuleViewModel
    {
        public int ModuleId { get; set; }

        [Required(ErrorMessage = "Поле Назва обов'язкове.")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool HasTest { get; set; }
        public bool HasLab { get; set; }
        public int? MinTestMark { get; set; }
        public int? MaxTestMark { get; set; }
        public int? MinLabMark { get; set; }
        public int? MaxLabMark { get; set; }
        public Subject Subject { get; set; }
        public int? SubjectId { get; set; }
        public TimeSpan? Duration { get; set; }

        public List<IFormFile> Files { get; set; }
        public int CountFiles { get; internal set; }

        public ModuleViewModel() {}

        public ModuleViewModel(Module module)
        {
            ModuleId = module.ModuleId;
            SubjectId = module.SubjectId;
            Subject = module.Subject;
            Name = module.Name;
            Description = module.Description;
            Duration = module.Duration;
            HasTest = module.HasTest;
            HasLab = module.HasLab;
            MinTestMark = module.MinTestMark;
            MaxTestMark = module.MaxTestMark;
            MinLabMark = module.MinLabMark;
            MaxLabMark = module.MaxLabMark;
            CountFiles = module.ModuleFile.Count;
        }
    }
}
