﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Videos
{
    public class VideoViewModel
    {
        [Required(ErrorMessage = "Посилання на відео обов'язкове.")]
        [Display(Name = "Посилання")]
        public string Link { get; set; }

        [Required(ErrorMessage = "Оберіть курс")]
        [Display(Name = "Курс")]
        public int CourseId { get; set; }
        public IEnumerable<SelectListItem> Courses { get; set; }


        [Required(ErrorMessage = "Обреіть модуль.")]
        [Display(Name = "Модуль")]
        public int CourseModuleId { get; set; }
        public IEnumerable<SelectListItem> CourseModules { get; set; }

        public int VideoId { get; set; }
        public int OldCourseModuleId { get; set; }
    }
}
