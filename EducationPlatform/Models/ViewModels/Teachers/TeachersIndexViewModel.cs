﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Teachers
{
    public class TeachersIndexViewModel
    {
        public List<AspNetUsers> Teachers { get; set; }
    }
}
