﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Marks
{
    public class MarkViewModel
    {
        public MarkViewModel(Mark mark)
        {
            MarkId = mark.MarkId;
            TestMark = mark.TestMark;
            LabMark = mark.LabMark;
            StudentId = mark.StudentId;
            CourseModuleId = mark.CourseModuleId;
        }

        public int MarkId { get; set; }
        public int? TestMark { get; set; }
        public int? LabMark { get; set; }
        public int StudentId { get; set; }
        public int CourseModuleId { get; set; }
    }
}
