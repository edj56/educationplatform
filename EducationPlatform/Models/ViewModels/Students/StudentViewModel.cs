﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Students
{
    public class StudentViewModel
    {
        public int StudentId { get; set; }
        public string UserId { get; set; }
        public string University { get; set; }
        public string Faculty { get; set; }
        public int? StudyYear { get; set; }
        public string Skills { get; set; }
        public bool? HasAccess { get; set; }
        public int? CourseNumber { get; set; }

        public UserViewModel User { get; set; }

        public StudentViewModel() { }

        public StudentViewModel(Student student)
        {
            StudentId = student.StudentId;
            UserId = student.UserId;
            University = student.University;
            Faculty = student.Faculty;
            StudyYear = student.StudyYear;
            Skills = student.Skills;
            HasAccess = student.HasAccess;
            CourseNumber = student.CourseStudent.Count();
            User = new UserViewModel(student.User);
        }
        public StudentViewModel(RegisterViewModel model)
        {
            University = model.University;
            Faculty = model.Faculty;
            StudyYear = model.StudyYear;
            Skills = model.Skills;
            HasAccess = false;
        }

        public StudentViewModel(StudentFormViewModel model)
        {
            University = model.University;
            Faculty = model.Faculty;
            StudyYear = model.StudyYear;
            Skills = model.Skills;
            HasAccess = model.HasAccess;
        }
    }
}
