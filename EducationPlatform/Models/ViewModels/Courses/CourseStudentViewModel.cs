﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Courses
{
    public class CourseStudentViewModel
    {

        public CourseStudentViewModel(CourseStudent courseStudent)
        {
            CourseStudentId = courseStudent.CourseStudentId;
            StudentId = courseStudent.StudentId;
            CourseId = courseStudent.CourseId;

            Student = new StudentViewModel(courseStudent.Student);
        }

        public int CourseStudentId { get; set; }
        public int? StudentId { get; set; }
        public int? CourseId { get; set; }

        public StudentViewModel Student { get; set; }
    }
}
