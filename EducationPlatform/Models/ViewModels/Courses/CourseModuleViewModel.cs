﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Courses
{
    public class CourseModuleViewModel
    {
        public CourseModuleViewModel(CourseModule courseModule)
        {
            CourseModuleId = courseModule.CourseModuleId;
            CourseId = courseModule.CourseId;
            ModuleId = courseModule.ModuleId;
            Date = courseModule.Date;
            StartTime = courseModule.StartTime;

            Module = new ModuleViewModel(courseModule.Module);
        }

        public int CourseModuleId { get; set; }
        public int CourseId { get; set; }
        public int ModuleId { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? StartTime { get; set; }

        public ModuleViewModel Module { get; set; }
    }
}
