﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Courses
{
    public class CourseDetailsModuleViewModel
    {
        public Module Module { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Selected { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan? StartTime { get; set; }
    }
}