﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Subjects;
using EducationPlatform.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Courses
{
    public class CourseViewModel
    {
        public int CourseId { get; set; }
        public int SubjectId { get; set; }
        public SubjectViewModel Subject { get; set; }
        public string TeacherId { get; set; }
        public UserViewModel Teacher { get; set; }
        [Required(ErrorMessage = "Назва курсу обов'язкова.")]
        public string Name { get; set; }
        public string Description { get; set; }
        [StartLessThanEnd("End", ErrorMessage = "Дата початку не може бути більшою за дату кінця.")]
        [Required(ErrorMessage = "Дата початку курсу обов'язкова.")]
        public DateTime Start { get; set; }
        [Required(ErrorMessage = "Дата кінця курсу обов'язкова.")]
        public DateTime End { get; set; }
        public bool HideMarks { get; set; }
        public ICollection<CourseModuleViewModel> CourseModule { get; set; }

        public CourseViewModel() {}

        public CourseViewModel(Course course)
        {
            CourseId = course.CourseId;
            Name = course.Name;
            TeacherId = course.TeacherId;
            SubjectId = course.SubjectId;
            Teacher = new UserViewModel(course.Teacher);
            Start = course.StartDate;
            End = course.EndDate;
            HideMarks = course.HideMarks;

            CourseModule = new List<CourseModuleViewModel>();
            foreach (var courseModule in course.CourseModule)
            {
                CourseModule.Add(new CourseModuleViewModel(courseModule));
            }
        }
    }
}
