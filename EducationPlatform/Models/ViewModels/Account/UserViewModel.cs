﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Students;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Account
{
    public class UserViewModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Поле Email обов'язкове.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string UserName { get; set; }

        [Required(ErrorMessage = "Поле Прізвище обов'язкове.")]
        [Display(Name = "Прізвище")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Поле Ім'я обов'язкове.")]
        [Display(Name = "Ім'я")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Поле По Батькові обов'язкове.")]
        [Display(Name = "По Батькові")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Поле Номер телефону обов'язкове.")]
        [Display(Name = "Номер телефону")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Поле Пароль обов'язкове.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        public bool? IsBanned { get; set; }

        public UserViewModel() { }

        public UserViewModel(AspNetUsers model)
        {
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
            MiddleName = model.MiddleName;
            PhoneNumber = model.PhoneNumber;
            UserName = model.Email;
            IsBanned = model.IsBanned;
        }

        public UserViewModel(RegisterViewModel model)
        {
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
            MiddleName = model.MiddleName;
            Password = model.Password;
            PhoneNumber = model.PhoneNumber;
            UserName = model.Email;
            IsBanned = false;
        }

        public UserViewModel(StudentFormViewModel model)
        {
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
            MiddleName = model.MiddleName;
            Password = model.Password;
            PhoneNumber = model.PhoneNumber;
            UserName = model.Email;
            IsBanned = model.IsBan;
        }

        public UserViewModel(User user)
        {
            Id = user.Id;
            Email = user.Email;
            UserName = user.UserName;
            LastName = user.LastName;
            FirstName = user.FirstName;
            MiddleName = user.MiddleName;
            PhoneNumber = user.PhoneNumber;
        }
    }
}
