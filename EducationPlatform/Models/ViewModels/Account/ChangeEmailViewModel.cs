﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Account
{
    public class ChangeEmailViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }

    }
}
