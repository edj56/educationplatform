﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Account
{
    public class AspNetUserLoginViewModel
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string ProviderDisplayName { get; set; }
        public string UserId { get; set; }
        public AspNetUserViewModel User { get; set; }

        public AspNetUserLoginViewModel() {}

        public AspNetUserLoginViewModel(AspNetUserLogins userLogins)
        {
            LoginProvider = userLogins.LoginProvider;
            ProviderKey = userLogins.ProviderKey;
            ProviderDisplayName = userLogins.ProviderDisplayName;
            UserId = userLogins.UserId;
        }
    }
}
