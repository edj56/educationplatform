﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Models.ViewModels.Account
{
    public class TwoFactorUserViewModel
    {
        public int TwoFactorUserId { get; set; }
        public string UserId { get; set; }
        public int? Code { get; set; }
        public AspNetUserViewModel User { get; set; }

        public TwoFactorUserViewModel() { }

        public TwoFactorUserViewModel(TwoFactorUser twoFactorUser)
        {
            TwoFactorUserId = twoFactorUser.TwoFactorUserId;
            UserId = twoFactorUser.UserId;
            Code = twoFactorUser.Code;
            User = new AspNetUserViewModel(twoFactorUser.User);
        }
    }
}
