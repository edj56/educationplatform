﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Models.ViewModels.Account;

namespace EducationPlatform.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IStudentsService _studentsService;
        private readonly IBotService _botService;

        public StudentsController
        (
            IUsersService usersService, 
            IStudentsService studentsService,
            IBotService botService
        )
        {
            _usersService = usersService;
            _studentsService = studentsService;
            _botService = botService;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index(string error = null, string message = null)
        {
            ViewBag.Error = error;
            ViewBag.Message = message;

            return View(await _studentsService.GetActiveStudentsAsync());
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(StudentFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _usersService.RegisterStudentAsync(
                          new UserViewModel(model),
                          new StudentViewModel(model)
                    );
                }
                catch
                {
                    ModelState.AddModelError("Email", "Ця електронна адреса вже зайнята!");
                    return View(model);
                }

                return RedirectToAction("Index", "Students", new { message = "Студент успішно доданий!" });
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(string id)
        {
            var stud = await _studentsService.GetByUserIdAsync(id);

            if (stud == null)
            {
                return RedirectToAction("Index", "Students", new { error = "Студента не знайдено!" });
            }

            StudentFormViewModel model = new StudentFormViewModel
            {
                Id = stud.User.Id,
                StudentId = stud.StudentId,
                LastName = stud.User.LastName,
                FirstName = stud.User.FirstName,
                MiddleName = stud.User.MiddleName,
                PhoneNumber = stud.User.PhoneNumber,
                University = stud.University,
                Faculty = stud.Faculty,
                Email = stud.User.Email,
                HasAccess = stud.HasAccess.Value,
                IsBan = stud.User.IsBanned.Value,
                Skills = stud.Skills,
                StudyYear = stud.StudyYear.HasValue ? stud.StudyYear.Value : 0
            };


            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Student")]
        public async Task<IActionResult> Edit(StudentFormViewModel model, int StudentId = -1)
        {
            if (ModelState.IsValid || ModelState.ErrorCount == 2)
            {
                var student = await _studentsService.GetByIdAsync(StudentId);
                var resU = await _usersService.UpdateAsync(new UserViewModel(model));
                var resS = await _studentsService.UpdateAsync(model, StudentId);

                if (resU && resS)
                {
                    if (User.IsInRole("Student"))
                    {
                        return RedirectToAction("Index", "Profile");
                    }

                    if (student.HasAccess.Value) 
                    {
                        return RedirectToAction("Index", "Students", new { message = "Студент успішно відредагований!" });
                    }
                    else
                    {
                        return RedirectToAction("Entrants", "Students", new { message = "Студент успішно відредагований!" });
                    }
                }
                else if (!resU && !resS)
                {
                    return RedirectToAction("Index", "Students", new { error = "Сталася невідома помилка при редагуванні студента!" });
                }
            }

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var res = await _studentsService.DeleteAsync(id);

                if (res)
                {
                    return RedirectToAction("Index", "Students", new { message = "Студент успішно видалений!" });
                }

                return RedirectToAction("Index", "Students", new { error = "При видаленні студента сталася невідома помилка!" });
            }
            catch
            {
                return RedirectToAction("Index", "Students", new { error = "Студент задіяний на курсі, спершу видаліть студента з курсу!" });
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GrantAccess(int id)
        {
            var res = await _studentsService.ChangeAccessAsync(id, true);

            if (res)
            {
                return RedirectToAction("Index", "Students", new { message = "Студент успішно підтверджений!" });
            }

            return RedirectToAction("Index", "Students", new { error = "Сталася невідома помилка при підтвердженні студента!" });
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RevokeAccess(int id)
        {
            var res = await _studentsService.ChangeAccessAsync(id, false);

            if (res)
            {
                return RedirectToAction("Index", "Students", new { message = "Ви успішно зняли доступ!" });
            }

            return RedirectToAction("Index", "Students", new { error = "Сталася невідома помилка при знятті доступу студента!" });
        }


        [HttpGet]
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Setting(string id)
        {
            try
            {
                var student = await _studentsService.GetByUserIdAsync(id);

                if (student == null)
                {
                    return RedirectToAction("Index", "Profile", new { error = "Студента не знайдено!" });
                }

                StudentFormViewModel model = new StudentFormViewModel
                {
                    LastName = student.User.LastName,
                    FirstName = student.User.FirstName,
                    MiddleName = student.User.MiddleName,
                    PhoneNumber = student.User.PhoneNumber,
                    University = student.University,
                    Faculty = student.Faculty,
                    Email = student.User.Email,
                    Skills = student.Skills,
                    StudyYear = student.StudyYear.HasValue ? student.StudyYear.Value : 0,
                    StudentId = student.StudentId,
                    HasAccess = student.HasAccess.Value
                };

                return View(model);
            }
            catch
            {
                return RedirectToAction("Index", "Profile", new { error = "Студента не знайдено!" });
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Entrants(string error = null, string message = null)
        {
            ViewBag.Error = error;
            ViewBag.Message = message;

            return View(await _studentsService.GetEntrantsAsync());
        }
    }
}