﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Bot;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using EducationPlatform.ValidationAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.Controllers
{
    [AuthorizeBot]
    public class BotController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IStudentsService _studentsService;
        private readonly IBotService _botService;
        private readonly IModulesService _modulesService;
        private readonly ICoursesService _coursesService;
        private readonly IMarksServices _marksService;

        public BotController
        (
            IUsersService usersService,
            IStudentsService studentsService,
            IBotService botService,
            IModulesService modulesService,
            ICoursesService coursesService,
            IMarksServices marksService
        )
        {
            _usersService = usersService;
            _studentsService = studentsService;
            _botService = botService;
            _modulesService = modulesService;
            _coursesService = coursesService;
            _marksService = marksService;
        }

        [HttpGet("/Bot/GetUserByKey/key={key}")]
        public async Task<IActionResult> GetUserByKey(string key)
        {
            BotAuthorizationKeysViewModel botKey = await _botService.GetUserByKey(key);
            
            if (botKey != null)
            {
                return Ok(botKey.UserId);
            }

            return NotFound();
        }

        [HttpGet("/Bot/GetStudentCourses/channelId={channelId}")]
        public async Task<IActionResult> GetStudentCourses(string channelId)
        {
            try
            {
                var userId = await _botService.GetAuthorizedUserId(channelId);
                var student = await _studentsService.GetByUserIdAsync(userId);
                var courses = await _studentsService.GetStudentCoursesAsync(student.StudentId);

                return Json(courses);
            }
            catch (Exception)
            {
                return NotFound("No courses have been found");
            }
        }

        [HttpPost("/Bot/{channelId}/{userId}/{channelUserId}/{conversationId}")]
        [Route("/Bot/{channelId}/{userId}/{channelUserId}/{conversationId}")]
        public async Task<IActionResult> CreateNewAuthorizedUser(string channelId, string userId, string channelUserId, string conversationId)
        {
            BotAuthorizedUsersViewModel botAuthorizedUser = await _botService.GetBotAuthorizedUser(userId);
            if (botAuthorizedUser != null)
            {
                await _botService.DeleteBotAuthorizedUser(botAuthorizedUser);
            }
            await _botService.CreateBotAuthorizedUser(channelId, userId, channelUserId, conversationId);
            return Ok();
        }

        [HttpGet("/Bot/CheckBotUserAuthorization/channelUserId={channelUserId}")]
        public async Task<IActionResult> CheckBotUserAuthorization(string channelUserId)
        {
            return Json(await _botService.CheckBotUserAuthorization(channelUserId));
        }

        [HttpGet("/Bot/GetAuthorizedUserRole/channelUserId={channelUserId}")]
        public async Task<IActionResult> GetAuthorizedUserRole(string channelUserId)
        {
            string role = await _usersService.GetUserRoleAsync(await _botService.GetAuthorizedUserId(channelUserId));

            if (role != null)
            {
                return Ok(role);
            }

            return NotFound();
        }

        [HttpPost("/Bot/DeleteAuthorizationKey/key={key}")]
        public async Task<IActionResult> DeleteAuthorizationKey(string key)
        {
            BotAuthorizationKeysViewModel botKey = await _botService.GetAuthorizationKeyByKey(key);

            if (botKey != null)
            {
                await _botService.DeleteAuthorizationKey(botKey);
                return Ok();
            }

            return NotFound();
        }

        [HttpGet("/Bot/GetStudentModuleMarks/channelId={channelId}&courseModuleId={courseModuleId}")]
        public async Task<IActionResult> GetStudentModuleMarks(string channelId, int courseModuleId)
        {
            try
            {
                var userId = await _botService.GetAuthorizedUserId(channelId);
                var student = await _studentsService.GetByUserIdAsync(userId);
                var marks = (await _marksService.GetMarksViewModelsByStudentIdAndModuleIdAsync(student.StudentId, courseModuleId)).ToList();

                return Json(marks);
            }
            catch (Exception)
            {
                return NotFound("No marks have been found");
            }
        }

        [HttpGet("/Bot/GetCourseStudents/courseId={courseId}")]
        public async Task<IActionResult> GetCourseStudents(int courseId)
        {
            try
            {
                var courseStudents = (await _coursesService.GetCourseStudentsAsync(courseId)).ToList();

                return Json(courseStudents);
            }
            catch (Exception)
            {
                return NotFound("No students assigned to this course");
            }
        }

        [HttpGet("/Bot/PlaceMark/courseModuleId={courseModuleId}&studentId={studentId}&mark={mark}&type={type}")]
        public async Task<IActionResult> PlaceMark(int courseModuleId, int studentId, int mark, string type)
        {
            try
            {
                var exsistingMarks = (await _marksService.GetMarksByStudentIdAndModuleIdAsync(studentId, courseModuleId)).ToList();
                if (type == "Test")
                {
                    if (exsistingMarks.Count < 1)
                    {
                        await _marksService.AddMarkAsync(new Mark { CourseModuleId = courseModuleId, StudentId = studentId, TestMark = mark });
                    }
                    else
                    {
                        await _marksService.PlaceTestMarkAsync(exsistingMarks[0], mark);
                    }
                }
                else if (type == "Lab")
                {
                    if (exsistingMarks.Count < 1)
                    {
                        await _marksService.AddMarkAsync(new Mark { CourseModuleId = courseModuleId, StudentId = studentId, LabMark = mark });
                    }
                    else
                    {
                        await _marksService.PlaceLabMarkAsync(exsistingMarks[0], mark);
                    }
                }
                else
                {
                    return Json(false);
                }
                return Json(true);
            }
            catch (Exception)
            {
                return NotFound("Error");
            }
        }

        [HttpGet("/Bot/GetMark/courseModuleId={courseModuleId}&studentId={studentId}&type={type}")]
        public async Task<IActionResult> GetMark(int courseModuleId, int studentId, string type)
        {
            try
            {
                var exsistingMarks = (await _marksService.GetMarksByStudentIdAndModuleIdAsync(studentId, courseModuleId)).ToList();

                if (exsistingMarks.Count < 1)
                {
                    return Json(false);
                }

                if (type == "Test")
                {
                    if (exsistingMarks[0].TestMark != null)
                    {
                        return Json(exsistingMarks[0].TestMark);
                    }
                }
                else if (type == "Lab")
                {
                    if (exsistingMarks[0].LabMark != null)
                    {
                        return Json(exsistingMarks[0].LabMark);
                    }
                }

                return Json(false);
            }
            catch (Exception)
            {
                return NotFound("Error");
            }
        }

        [HttpGet("/Bot/GetAllNextLectures/")]
        public async Task<IActionResult> GetAllNextLectures()
        {
            return Json(await _modulesService.GetAllNextCourseModules());
        }

        [HttpGet("/Bot/GetAllNextCourseModulesAvaliableForStudent/channelId={channelId}")]
        public async Task<IActionResult> GetAllNextLectures(string channelId)
        {
            try
            {
                return Json(await _modulesService.GetAllNextCourseModulesAvaliableForStudent(channelId));
            }
            catch (Exception)
            {
                return NotFound("No lectures found");
            }
        }

        [HttpGet("/Bot/GetTeacherCourses/channelId={channelId}")]
        public async Task<IActionResult> GetTeacherCourses(string channelId)
        {
            try
            {
                var userId = await _botService.GetAuthorizedUserId(channelId);
                var teacher = await _usersService.GetByIdAsync(userId);
                var courses = (await _coursesService.GetTeacherCoursesAsync(teacher.Id)).ToList();

                return Json(courses);
            }
            catch (Exception)
            {
                return NotFound("No courses have been found");
            }
        }
    }
}