﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        private readonly IModulesService _modulesService;
        private readonly ICoursesService _coursesService;

        public CalendarController(
            IModulesService modulesService,
            ICoursesService coursesService
        )
        {
            _modulesService = modulesService;
            _coursesService = coursesService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<IActionResult> CourseSchedule(int id)
        {
            var schedule = await _coursesService.GetByIdAsync(id);

            if (schedule != null)
            {
                var json = schedule.CourseModule.Select(x => new
                {
                    module = _modulesService.GetById(x.ModuleId),
                    start = (x.Date.HasValue ? x.Date.Value.ToString("yyyy-MM-dd") : DateTime.MinValue.ToString("yyyy-MM-dd")) + " " + x.StartTime,
                    end = (x.Date.HasValue ? x.Date.Value.ToString("yyyy-MM-dd") : DateTime.MinValue.ToString("yyyy-MM-dd")) + " " + (x.StartTime + (x.Module.Duration != null ? x.Module.Duration : TimeSpan.FromHours(2)))
                }).Select(async x => new
                {
                    title = (await x.module).Name,
                    description = (await x.module).Description,
                    x.start,
                    x.end
                }).ToList();

                return Json(json);
            }

            return Json(new List<object>());
        }
    }
}