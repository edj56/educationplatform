﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EducationPlatform.Models.Entities;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Teachers;
using Microsoft.AspNetCore.Identity;

namespace EducationPlatform.Controllers
{
    public class TeachersController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IBotService _botService;
        private readonly ICoursesService _coursesService;
        private readonly IUsersService _userService;
        public TeachersController
        (
            IBotService botService,
            ICoursesService coursesService,
            IUsersService userService,
            UserManager<User> userManager
        )
        {
            _botService = botService;
            _coursesService = coursesService;
            _userService = userService;
            _userManager = userManager;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index(string error = null, string message = null)
        {
            TeachersIndexViewModel list = new TeachersIndexViewModel
            {
                Teachers = (await _userService.GetTeachers()).ToList()
            };

            if (error != null || message != null)
            {
                ViewBag.Error = error;
                ViewBag.Message = message;
            }
            return View(list);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            UserViewModel model = new UserViewModel
            {
                IsBanned = false
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var res = await _userService.RegisterTeacherAsync(model);

                    if (!res)
                    {
                        ModelState.AddModelError("Email", "Ця електронна адреса вже зайнята!");
                        return View(model);
                    }
                    return RedirectToAction("Index", "Teachers", new { message = "Викладач успішно створений!" });
                }
                catch
                {
                    ModelState.AddModelError("Email", "Ця електронна адреса вже зайнята!");
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id != null)
            {
                User user = await _userManager.FindByIdAsync(id);

                if (user == null)
                {
                    return RedirectToAction("Index", "Teachers");
                }

                UserViewModel teacher = new UserViewModel
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    LastName = user.LastName,
                    FirstName = user.FirstName,
                    MiddleName = user.MiddleName,
                    PhoneNumber = user.PhoneNumber,
                    IsBanned = (bool)user.IsBanned
                };

                return View(teacher);
            }
            return RedirectToAction("Index", "Teachers");
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<IActionResult> Edit(UserViewModel model)
        {
            var res = await _userService.UpdateAsync(model);
            var user = await _userManager.GetUserAsync(User);
            if (res && (ModelState.IsValid || ModelState.ErrorCount == 1))
            {
                if (User.IsInRole("Admin"))
                {
                    if (user.Email == model.Email)
                    {
                        return RedirectToAction("Index", "Profile", new { message = "Профіль успішно відредагований!" });
                    }

                    return RedirectToAction("Index", "Teachers", new { message = "Викладач успішно відредагований!" });
                }

                return RedirectToAction("Index", "Profile", new { message = "Профіль успішно відредагований!" });
            }
            else
            {
                if (User.IsInRole("Admin"))
                {
                    if (user.Email == model.Email)
                    {
                        return RedirectToAction("Index", "Profile", new { error = "Сталася невідома помилка при редагуванні профілю!" });
                    }

                    return RedirectToAction("Index", "Teachers", new { error = "Сталася невідома помилка при редагуванні викладача!" });
                }

                return RedirectToAction("Index", "Profile", new { error = "Сталася невідома помилка при редагуванні профілю!" });
            }
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string email)
        {
            try
            {
                var res = await _userService.DeleteUserByEmailAsync(email);

                if (res)
                {
                    return Json(new { url = Url.Action("Index", "Teachers", new { message = "Викладач успішно видалений!" }) });
                }
                else
                {
                    return Json(new { url = Url.Action("Index", "Teachers", new { error = "Сталася невідома помилка при видаленні викладача!" }) });
                }
            }
            catch
            {
                return Json(new { url = Url.Action("Index", "Teachers", new { error = "Даний викладач уже викладає на курсах, спершу змініть викладача на існуючих курсах!" }) });
            }


        }

        [HttpGet]
        [Authorize(Roles = "Admin, Teacher")]
        [Route("{controller}/{action}/{id}")]
        public async Task<IActionResult> Setting(string id)
        {
            string role = "";
            var isAdmin = User.IsInRole("Admin");

            if (isAdmin)
            {
                role = "Admin";
            }
            else
            {
                role = "Teacher";
            }

            if (id != null)
            {
                User user = await _userManager.FindByIdAsync(id);

                if (user == null)
                {
                    return RedirectToAction("Index", "Profile", new { error = "Користувача не знайдено!" });
                }

                ViewBag.UserRole = await _userService.GetUserRoleAsync(id);
                return View(new UserViewModel(user));
            }

            return RedirectToAction(role, "Profile", new { error = "Користувача не знайдено!" });
        }

        [HttpGet("/Teachers/GetTeacherCourses/channelId={channelId}")]
        public async Task<IActionResult> GetTeacherCourses(string channelId)
        {
            try
            {
                var userId = await _botService.GetAuthorizedUserId(channelId);
                var teacher = await _userService.GetByIdAsync(userId);
                var courses = (await _coursesService.GetTeacherCoursesAsync(teacher.Id)).ToList();

                // Remove self-loop
                //for (int i = 0; i < courses.Count; i++)
                //{
                //    for (int j = 0; j < courses[i].CourseModule.Count; j++)
                //    {
                //        courses[i].CourseModule.ToList()[j].Course = null;
                //        courses[i].CourseModule.ToList()[j].Module.CourseModule = null;
                //    }
                //    // courses[i].CourseStudent = null;
                //}

                return Json(courses);
            }
            catch (Exception)
            {
                return NotFound("No courses have been found");
            }
        }

        [HttpGet("/Teachers/GetCurrentUserRole")]
        [Authorize(Roles ="Admin, Teacher, Student")]
        public async Task<IActionResult> GetCurrentUserRole()
        {
            var user = await _userService.GetAspNetUserByEmailAsync(User.Identity.Name);
            var result = await _userService.GetUserRoleAsync(user.Id);
            return Json(result);
        }
    }
}