﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EducationPlatform.Models.Entities;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IUsersService _usersService;
        private readonly IBotService _botService;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IEmailSender emailSender,
            IUsersService usersService,
            IBotService botService
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _usersService = usersService;
            _botService = botService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Profile");
            }

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _usersService.RegisterStudentAsync(
                        new UserViewModel(model),
                        new StudentViewModel(model)
                    );
                }
                catch
                {
                    ModelState.AddModelError("Email", "Ця електронна адреса вже зайнята!");
                    return View(model);
                }

                return RedirectToAction("RegisterSuccess", "Account");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null, string error = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Courses");
            }

            if (error != null)
            {
                ModelState.AddModelError("", error);
            }
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    if (user.IsBanned.HasValue && user.IsBanned.Value == true)
                    {
                        await _signInManager.SignOutAsync();

                        return RedirectToAction("Banned", "Account");
                    }

                    var twoFactor = await _usersService.GetTwoFactorUserByIdAsync(user.Id);

                    if (twoFactor != null)
                    {
                        await _signInManager.SignOutAsync();

                        Random rand = new Random();
                        int code = rand.Next(1000, 10000);

                        twoFactor.Code = code;

                        var callbackUrl = Url.Action("CheckValidationCode", "Account", new { email = user.Email }, protocol: HttpContext.Request.Scheme);

                        await _usersService.UpdateTwoFactorUserAsync(twoFactor);
                        await _emailSender.SendDefaultEmailAsync(user.Email, "Код авторизації", $"{twoFactor.Code}", callbackUrl, "Перейти до системи");
                        return RedirectToAction("CheckValidationCode", "Account", new { email = user.Email });
                    }

                    return RedirectToAction("Index", "Profile");
                }

                ModelState.AddModelError("", "Неправильний логін чи пароль!");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Banned()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string code, string email)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }

            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (result.Succeeded)
            {
                await _emailSender.SendDefaultEmailAsync(email, "Реєстрація", $"Аккаунт упішно підтверджений!", $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToString()}/", "Перейти до системи");
            }

            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        public IActionResult CheckValidationCode(string email)
        {
            ViewBag.Email = email;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> TwoFactorAuth(TwoFactorViewModel model)
        {
            var twofactor = await _usersService.GetTwoFactorUserByIdAsync((await _userManager.FindByEmailAsync(model.Email)).Id);

            if (twofactor != null)
            {
                if (twofactor.Code.HasValue)
                {
                    if (twofactor.Code.Value == model.Code)
                    {
                        await _signInManager.SignInAsync(await _userManager.FindByEmailAsync(model.Email), true);
                        return RedirectToAction("Index", "Profile");
                    }
                }
            }

            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> SetTwoFactorAuthorization(bool grant)
        {
            var user = await _userManager.GetUserAsync(User);
            var twofactoruser = await _usersService.GetTwoFactorUserByIdAsync(user.Id);

            if (grant == true)
            {
                if (twofactoruser == null)
                {
                    await _usersService.AddTwoFactorUserAsync(user.Id);
                }
            }
            else
            {
                if (twofactoruser != null)
                {
                    await _usersService.DeleteTwoFactorUserAsync(twofactoruser);
                }
            }

            return RedirectToAction("Index", "Profile");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword(string error = "")
        {
            ViewBag.Error = error;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return RedirectToAction("ForgotPassword", new { error = "Користувач з таким email не знайдений." });
                }
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);

                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code, email = user.Email }, protocol: HttpContext.Request.Scheme);
                await _emailSender.SendDefaultEmailAsync(model.Email, "Відновлення паролю", "Щоб відновити пароль перейдіть за посиланням:", callbackUrl, "Відновити!");

                return View("ForgotPasswordSuccess");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string email, string code = null)
        {
            if (code == null)
            {
                return RedirectToAction("ForgotPassword", new { error = "Виникла проблема із посиланням, спробуйте ще раз." });
            }

            var model = new ResetPasswordViewModel { Code = code, Email = email };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model, string code = null)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            model.Email = user.Email;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (user == null)
            {
                return RedirectToAction("ForgotPassword", new { error = "Користувач з таким email не знайдений." });
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);

            if (result.Succeeded)
            {
                await _emailSender.SendDefaultEmailAsync(model.Email, "Відновлення паролю", $"Ви успішно змінили свій пароль!", $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToString()}/", "Перейти до системи");
                return RedirectToAction("ResetPasswordSuccess", "Account");
            }

            ViewBag.Errors = result.Errors.Select(x => x.Description).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult ForgotPasswordSuccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ResetPasswordSuccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult RegisterSuccess()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Block(string id, string page)
        {
            var result = await _usersService.BlockUser(id);
            if (!result)
            {
                return RedirectToAction("Index", page, new { error = "Користувача не вдалося заблокувати!" });
            }
            return RedirectToAction("Index", page, new { message = "Користувач успішно заблокований!" });
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnBlock(string id, string page)
        {
            var result = await _usersService.UnBlockUser(id);
            if (!result)
            {
                return RedirectToAction("Index", page, new { error = "Користувача не вадлося розблокувати!" });
            }
            return RedirectToAction("Index", page, new { message = "Користувач успішно розблокований!" });
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Student,Teacher")]
        public async Task<IActionResult> SubmitEmail(ChangeEmailViewModel model)
        {
            //Захист від того щоб студент або викладач не могли через URL запит змінити Email іншому користувачу
            if (!User.IsInRole("Admin"))
            {
                model.Id = _userManager.GetUserId(User);
            }



            User user = await _userManager.FindByIdAsync(model.Id);

            if (User.IsInRole("Admin") && await _usersService.GetUserRoleAsync(user.Id) == "Admin")
            {
                return Json(new { error = true, message = "Неможливо змінити Email адміністратору" });
            }

            if (await _usersService.GetAspNetUserByEmailAsync(model.Email) == null)
            {
                await _usersService.ChangeEmail(user, model.Email);

            }
            else
            {
                return Json(new { error = true, message = "Email вже зайнятий" });
            }

            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin"))
                {
                    if (await _userManager.IsInRoleAsync(user, "Student"))
                    {
                        return Json(new { url = Url.Action("Edit", "Students", new { id = user.Id }) });
                    }
                    if (await _userManager.IsInRoleAsync(user, "Teacher"))
                    {
                        return Json(new { url = Url.Action("Edit", "Teachers", new { id = user.Id }) });
                    }

                    return Json(new { url = Url.Action("Setting", "Teachers", new { id = user.Id }) });
                }

                if (User.IsInRole("Student"))
                {
                    return Json(new { url = Url.Action("Setting", "Students", new { id = user.Id }) });
                }

                if (User.IsInRole("Teacher"))
                {
                    return Json(new { url = Url.Action("Setting", "Teachers", new { id = user.Id }) });
                }
            }
            return RedirectToAction("Error", "Home");
        }

    }
}