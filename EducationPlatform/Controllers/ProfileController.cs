﻿using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.Entities;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.Controllers
{
    [Authorize(Roles = "Admin, Teacher, Student")]
    public class ProfileController : Controller
    {
        private readonly IStudentsService _studentService;
        private readonly IUsersService _usersService;
        private readonly UserManager<User> _userManager;
        private readonly IBotService _botService;
        private readonly ICoursesService _coursesService;

        public ProfileController
        (
            IUsersService usersService,
            IStudentsService studentService,
            UserManager<User> userManager,
            IBotService botService,
            ICoursesService coursesService
        )
        {
            _usersService = usersService;
            _studentService = studentService;
            _userManager = userManager;
            _botService = botService;
            _coursesService = coursesService;
        }

        public async Task<IActionResult> Index(bool isModalWindowShowRequired = false, string error = null, string message = null)
        {
            string id = _userManager.GetUserId(User);

            ViewBag.HasTwoFactor = await _usersService.GetTwoFactorUserByIdAsync(id) != null;
            ViewBag.BotAuthorizationKey = await _botService.GetAuthorizationKey(id);
            ViewBag.IsModalWindowShowRequired = isModalWindowShowRequired;

            if (error != null || message != null)
            {
                ViewBag.Error = error;
                ViewBag.Message = message;
            }

            if (User.IsInRole("Teacher"))
            {
                ViewBag.Courses = (await _coursesService.GetTeacherCoursesAsync(id)).Count();
            }

            if (User.IsInRole("Student"))
            {
                StudentViewModel user = await _studentService.GetByUserIdAsync(id);
                return View("Student", user);
            }
            else
            {
                User user = await _userManager.FindByIdAsync(id);

                if (User.IsInRole("Teacher"))
                {
                    return View("Teacher", user);
                }
                else
                {
                    return View("Admin", user);
                }
            }
        }

        public async Task<IActionResult> GeneradeBotKey(string id)
        {
            if(await _botService.GetAuthorizationKey(id) == null)
            {
                await _botService.CreateAuthorizationKey(id);
            }

            return RedirectToAction("Index", new { isModalWindowShowRequired = true });
        }
    }
}