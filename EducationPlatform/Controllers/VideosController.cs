﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Videos;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace EducationPlatform.Controllers
{
    public class VideosController : Controller
    {
        private readonly IVideosService _videosService;
        private readonly ICoursesService _coursesService;

        public VideosController(IVideosService videosService, ICoursesService coursesService)
        {
            _videosService = videosService;
            _coursesService = coursesService;
        }

        [Route("{controller}")]
        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<IActionResult> Index(string error = null, string message = null)
        {
            ViewBag.Error = error;
            ViewBag.Message = message;

            return View(await _coursesService.GetCoursesAsync());
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> AddVideo(string error = null)
        {
            ViewBag.Error = error;
            return View(await _videosService.GetVideoViewModelAsync());
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> AddVideo(VideoViewModel model)
        {
            Regex regex = new Regex(@"^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})?$");

            if (ModelState.IsValid && regex.IsMatch(model.Link))
            {
                try
                {
                    await _videosService.AddVideoAsync(model);
                    return RedirectToAction("Index", "Videos", new { message = "Запис успішно додано!" });
                }
                catch
                {
                    return RedirectToAction("AddVideo", "Videos", new { error = "При додаванні запису виникла помилка! Перевірте посилання та спробуйте ще раз." });
                }
            }

            return RedirectToAction("AddVideo", "Videos", new { error = "При додаванні запису виникла помилка! Перевірте посилання та спробуйте ще раз." });
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseModuleId}/{videoId}")]
        public async Task<IActionResult> Edit(int videoId, int courseModuleId, string error = null)
        {
            ViewBag.Error = error;
            return View(await _videosService.GetVideoViewModelAsync(videoId, courseModuleId));
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseModuleId}/{videoId}")]
        public async Task<IActionResult> Edit(VideoViewModel model)
        {
            Regex regex = new Regex(@"^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})?$");

            if (ModelState.IsValid && regex.IsMatch(model.Link))
            {
                try
                {
                    await _videosService.EditVideoAsync(model);
                    return RedirectToAction("Index", "Videos", new { message = "Запис успішно змінено!" });
                }
                catch
                {
                    return RedirectToAction("Index", "Videos", new { error = "При редагуванні запису виникла помилка! Перевірте посилання та спробуйте ще раз." });
                }
            }

            return RedirectToAction("Index", "Videos", new { error = "При редагуванні запису виникла помилка! Перевірте посилання та спробуйте ще раз." });
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Delete(int videoId, int courseModuleId)
        {
            try
            {
                await _videosService.DeleteVideoAsync(videoId, courseModuleId);
                return Json(new { url = Url.Action("Index", "Videos", new { message = "Запис успішно видалено!" }) });
            }
            catch
            {
                return Json(new { url = Url.Action("Index", "Videos", new { error = "Під час видалення відео сталася помилка!" }) });
            }

        }

        [HttpGet]
        public async Task<ActionResult> GetCourseModules(string id)
        {
            if (id != null)
            {
                return (await _videosService.GetModulesForDdropdownAsync(id));
            }
            else
            {
                return null;
            }
        }
    }
}