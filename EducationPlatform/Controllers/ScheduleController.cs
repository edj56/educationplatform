﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.Controllers
{
    public class ScheduleController : Controller
    {
        private readonly IScheduleService _scheduleService;
        private readonly ICoursesService _coursesService;

        public ScheduleController(IScheduleService scheduleService, ICoursesService coursesService)
        {
            _scheduleService = scheduleService;
            _coursesService = coursesService;
        }

        [Authorize(Roles = "Admin, Student, Teacher")]
        public async Task<IActionResult> Index()
        {
            CoursesIndexViewModel coursesIndexViewModel = new CoursesIndexViewModel()
            {
                Courses = (await _coursesService.GetCoursesAsync()).ToList()
            };
            return View(coursesIndexViewModel);
        }

        [Authorize(Roles = "Admin, Student, Teacher")]
        [Route("{controller}/{action}")]
        public async Task<IActionResult> GetAllCoursesSchedule()
        {
            var schedule = await _scheduleService.GetScheduleByAllCourses();
            if (schedule != null)
            {
                return Json(schedule);
            }
            return Json(new List<object>());
        }


        [Authorize(Roles = "Student")]
        [Route("{controller}/{action}/{id}")]
        public async Task<IActionResult> GetScheduleForStudent(string Id)
        {
            var schedule = await _scheduleService.GetScheduleByStudent(Id);
            if (schedule != null)
            {
                return Json(schedule);
            }
            return Json(new List<object>());

        }

        [Authorize(Roles = "Teacher")]
        [Route("{controller}/{action}/{email}")]
        public async Task<IActionResult> GetTeacherSchedule(string email)
        {
            var schedule = await _scheduleService.GetScheduleByTeacher(email);
            if (schedule != null)
            {
                return Json(schedule);
            }
            return Json(new List<object>());
        }


        [Authorize(Roles = "Admin")]
        [Route("{controller}/{action}")]
        public async Task<IActionResult> GetCoursesScheduleByFilter(List<int> coursesId)
        {
            var coursesSchedule = await _scheduleService.GetCoursesSchedulebyFilter(coursesId);
            if (coursesSchedule != null)
            {
                return Json(coursesSchedule);
            }
            return Json(new List<object>());
        }


        [Authorize(Roles = "Admin")]
        [Route("{controller}/{action}")]
        public async Task<IActionResult> GetCoursesScheduleByTeacheFilter(List<string> emails)
        {
            var coursesSchedule = await _scheduleService.GetCoursesScheduleByTeacherFilter(emails);
            if (coursesSchedule != null)
            {
                return Json(coursesSchedule);
            }
            return Json(new List<object>());
        }
    }
}