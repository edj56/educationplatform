using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Subjects;
using EducationPlatform.Models.ViewModels.Bot;
using EducationPlatform.Models.ViewModels.Modules;

namespace EducationPlatform.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ICoursesService _coursesService;
        private readonly ISubjectsService _subjectsService;
        private readonly IModulesService _modulesService;
        private readonly IStudentsService _studentsService;
        private readonly IEmailSender _emailSender;
        private readonly IBotService _botService;
        private readonly UserManager<User> _userManager;
        private readonly IMarksServices _marksServices;
        private readonly IUsersService _usersService;

        public CoursesController(
            ISubjectsService subjectsService,
            ICoursesService coursesService,
            IModulesService modulesService,
            IStudentsService studentsService,
            IEmailSender emailSender,
            UserManager<User> userManager,
            IMarksServices marksServices,
            IBotService botService,
            IUsersService usersService
        )
        {
            _subjectsService = subjectsService;
            _modulesService = modulesService;
            _modulesService = modulesService;
            _studentsService = studentsService;
            _emailSender = emailSender;
            _userManager = userManager;
            _coursesService = coursesService;
            _marksServices = marksServices;
            _botService = botService;
            _usersService = usersService;
        }

        [Route("{controller}")]
        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<IActionResult> Index(string message = null, string error = null)
        {
            ViewBag.Message = message;
            ViewBag.Error = error;


            CoursesIndexViewModel coursesIndexViewModel = new CoursesIndexViewModel()
            {
                Courses = (await _coursesService.GetCoursesAsync()).ToList()
            };
            return View(coursesIndexViewModel);
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Create()
        {
            IEnumerable<SubjectViewModel> subjects = await _subjectsService.GetSubjectsAsync();
            IEnumerable<AspNetUsers> teachers = await _usersService.GetNotBannedUsersInRole("Teacher");
            AspNetUsers me = teachers.FirstOrDefault(x => x.Id == _userManager.GetUserId(User));
            CourseViewModel model = new CourseViewModel();
            model.Start = DateTime.Now.Date;
            model.End = DateTime.Now.Date;

            if (me != null)
            {
                model.TeacherId = me.Id;
                model.Teacher = new UserViewModel { FirstName = me.FirstName, LastName = me.LastName };
            }

            ViewData["Subjects"] = subjects;
            ViewData["Teachers"] = teachers;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Create(CourseViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Course course = new Course()
                    {
                        CourseId = model.CourseId,
                        SubjectId = model.SubjectId,
                        Name = model.Name,
                        TeacherId = model.TeacherId,
                        StartDate = model.Start,
                        EndDate = model.End,
                        HideMarks = model.HideMarks
                    };
                    await _coursesService.InsertAsync(course);
                    return RedirectToAction("Index", "Courses", new { message = "Курс успішно додано!" });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Courses", new { error = "При створенні курсу виникла невідома помилка!" });
            }
            return RedirectToAction("Index", "Courses", new { error = "При створенні курсу виникла невідома помилка!" });
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> Details(int courseId)
        {
            Course courseFromDB = await _coursesService.GetByIdAsync(courseId);
            SubjectViewModel subjectFromDB = await _subjectsService.GetByIdAsync(courseFromDB.Subject.SubjectId);

            var modulesFromDB = await _coursesService.GetScheduleModulesForDetailsAsync(courseId);

            List<Student> students = (await _coursesService.GetStudentsByCourseIdAsync(courseId)).ToList();
            User userFromDB = await _userManager.FindByEmailAsync(courseFromDB.Teacher.Email);
            CourseDetailsViewModel courseDetailsViewModel = new CourseDetailsViewModel()
            {
                Course = courseFromDB,
                Subject = new Subject()
                {
                    SubjectId = subjectFromDB.SubjectId,
                    Name = subjectFromDB.Name,
                    Description = subjectFromDB.Description
                },
                Teacher = new AspNetUsers()
                {
                    Id = userFromDB.Id,
                    FirstName = userFromDB.FirstName,
                    LastName = userFromDB.LastName,
                    MiddleName = userFromDB.MiddleName
                },
                Modules = modulesFromDB.ToList(),
                Students = students
            };
            return View(courseDetailsViewModel);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> Edit(int courseId)
        {
            User teacher = await _userManager.FindByNameAsync(User.Identity.Name);

            Course course = await _coursesService.GetByIdAsync(courseId);
            ViewData["Subjects"] = await _subjectsService.GetSubjectsAsync();
            ViewData["Teachers"] = await _usersService.GetNotBannedUsersInRole("Teacher");

            if (teacher.Id == course.TeacherId || User.IsInRole("Admin"))
            {

                CourseViewModel model = new CourseViewModel
                {
                    CourseId = course.CourseId,
                    Name = course.Name,
                    TeacherId = course.TeacherId,
                    SubjectId = course.SubjectId,
                    Teacher = new UserViewModel
                    {
                        UserName = course.Teacher.UserName,
                        FirstName = course.Teacher.FirstName,
                        LastName = course.Teacher.LastName,
                        MiddleName = course.Teacher.MiddleName,
                        IsBanned = course.Teacher.IsBanned.Value,
                        Email = course.Teacher.Email,
                        PhoneNumber = course.Teacher.PhoneNumber
                    },
                    Start = course.StartDate,
                    End = course.EndDate,
                    HideMarks = course.HideMarks
                };
                return View(model);
            }

            return RedirectToAction("Index", "Courses", new { error = "Ви можете редагувати тільки свої курси." });
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> Edit(CourseViewModel model)
        {
            if (ModelState.IsValid)
            {
                var oldCourse = await _coursesService.GetByIdAsync(model.CourseId);

                if (oldCourse.SubjectId != model.SubjectId)
                {
                    await _coursesService.RemoveModulesFromCourseAsync(model.CourseId);
                }

                oldCourse.SubjectId = model.SubjectId;
                oldCourse.Name = model.Name;
                oldCourse.TeacherId = model.TeacherId;
                oldCourse.StartDate = model.Start;
                oldCourse.EndDate = model.End;
                oldCourse.HideMarks = model.HideMarks;

                await _coursesService.UpdateAsync(oldCourse);

                return RedirectToAction("Index", "Courses", new { message = "Ви успішно відредагували свій курс!" });
            }
            return View(model);
        }

        [Authorize(Roles = "Teacher,Student")]
        public async Task<IActionResult> GetUserCourses()
        {
            var user = await _userManager.GetUserAsync(User);

            CoursesIndexViewModel model;

            if (User.IsInRole("Student"))
            {
                model = await _coursesService.GetStudentCoursesByIdAsync((await _studentsService.GetByUserIdAsync(user.Id)).StudentId);
            }
            else
            {
                model = await _coursesService.GetTeacherCoursesByIdAsync(user.Id);
            }

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [Route("{controller}/{action}/{studentId}")]
        public async Task<IActionResult> GetStudentCourses(int studentId)
        {
            return View(await _coursesService.GetStudentCoursesByIdAsync(studentId));
        }

        [Authorize(Roles = "Admin")]
        [Route("{controller}/{action}/{teacherId}")]
        public async Task<IActionResult> GetTeacherCourses(string teacherId)
        {
            return View(await _coursesService.GetTeacherCoursesByIdAsync(teacherId));
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditModules(int courseId)
        {
            var course = await _coursesService.GetByIdAsync(courseId);
            var subjectId = course.SubjectId;
            var subject = await _subjectsService.GetByIdAsync(subjectId);
            var modules = await _modulesService.GetModulesBySubjectId(subjectId);


            var model = new CourseEditModulesViewModel
            {
                CourseId = courseId,

                Course = new CourseViewModel
                {
                    CourseId = courseId,
                    Description = subject.Description,
                    Start = course.StartDate,
                    End = course.EndDate,
                    Subject = subject,
                    Name = course.Name,
                    Teacher = new UserViewModel
                    {
                        Email = course.Teacher.Email,
                        FirstName = course.Teacher.FirstName,
                        LastName = course.Teacher.LastName,
                        MiddleName = course.Teacher.MiddleName,
                    },
                    TeacherId = course.TeacherId,
                    SubjectId = subjectId
                },
                SubjectId = subjectId,
            };

            var modulesInCourses = (await _coursesService.GetModulesByCourseIdAsync(model.CourseId)).Select(x => x.ModuleId).ToList();

            model.Modules = modules.Select(x => new CourseModuleEditViewModel
            {
                Id = x.ModuleId,
                Name = x.Name,
                Description = x.Description,
                Selected = modulesInCourses.Count == 0 ? true : modulesInCourses.Contains(x.ModuleId)
            }).ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditModules(CourseEditModulesViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var result = await _coursesService.EditModulesAsync(model);

                    if (result)
                    {
                        // success
                        return RedirectToAction("Index", "Courses", new { message = "Ви успішно відредагували модуль!" });
                    }
                }
                catch (Exception ex)
                {
                    // error
                    ModelState.AddModelError(string.Empty, "Невідома помилка");
                    return View(model);

                }

            }
            else
            {
                ModelState.TryAddModelError(string.Empty, "Невідома помилка!");

            }
            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditStudents(int courseId)
        {
            var course = await _coursesService.GetByIdAsync(courseId);
            var students = await _studentsService.GetActiveNonBannedStudentsAsync();

            var subjectId = course.SubjectId;
            var subject = await _subjectsService.GetByIdAsync(subjectId);

            var model = new CourseEditStudentsViewModel
            {
                CourseId = courseId,

                Course = new CourseViewModel
                {
                    CourseId = courseId,
                    Description = subject.Description,
                    Start = course.StartDate,
                    End = course.EndDate,
                    Subject = subject,
                    Name = course.Name,
                    Teacher = new UserViewModel
                    {
                        Email = course.Teacher.Email,
                        FirstName = course.Teacher.FirstName,
                        LastName = course.Teacher.LastName,
                        MiddleName = course.Teacher.MiddleName,
                    },
                    TeacherId = course.TeacherId,
                    SubjectId = subjectId
                },
            };

            var studentsInCourse = (await _coursesService.GetStudentsByCourseIdAsync(model.CourseId)).Select(x => x.StudentId).ToList();

            model.Students = students.Select(x => new CourseStudentEditViewModel
            {
                Id = x.StudentId,
                Name = x.User.LastName + " " + x.User.FirstName + " " + x.User.MiddleName,
                Selected = studentsInCourse.Count == 0 ? false : studentsInCourse.Contains(x.StudentId)
            }).ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditStudents(CourseEditStudentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _coursesService.EditStudentsAsync(model);

                    if (result)
                    {
                        // success
                        return RedirectToAction("Index", "Courses", new { message = "Редагування к-сті студентів успішно виконано." });
                    }
                }
                catch
                {
                    // error
                    return RedirectToAction("Index", "Courses", new { error = "При редагуванні к-сті студентів сталась невідома помилка!" });
                }
            }
            else
            {
                ModelState.TryAddModelError(string.Empty, "Невідома помилка!");
            }
            return RedirectToAction("EditStudents", "Courses");
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditSchedule(int courseId, string error = null)
        {
            ViewBag.Error = error;

            var course = await _coursesService.GetByIdAsync(courseId);
            var modules = await _coursesService.GetScheduleModulesAsync(courseId);
            var subject = await _subjectsService.GetByIdAsync(course.SubjectId);

            var model = new CourseEditSchedulesViewModel
            {
                CourseId = courseId,

                Course = new CourseViewModel
                {
                    CourseId = courseId,
                    Start = course.StartDate,
                    End = course.EndDate,
                    Subject = subject,
                    Name = course.Name,
                    Teacher = new UserViewModel
                    {
                        Email = course.Teacher.Email,
                        FirstName = course.Teacher.FirstName,
                        LastName = course.Teacher.LastName,
                        MiddleName = course.Teacher.MiddleName,
                    },
                    TeacherId = course.TeacherId,
                    SubjectId = course.SubjectId

                },
            };

            model.Modules = modules.ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> EditSchedule(CourseEditSchedulesViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   // PushNotificationsViewModel pushNotificationsViewModel = await _botService.PrepareForPushNotifications(model);
                    var errors = await _coursesService.EditSchedulesAsync(model);

                    if (errors == null)
                    {
                        // success
                        try
                        {
                           // await _botService.SendPushNotificationsAsync(pushNotificationsViewModel);
                        }
                        catch
                        {
                            return RedirectToAction("Index", "Courses", new { message = "Редагування розкладу успішно виконано.", error = "Чат-бот недоступний для розсилання push-повідомленнь." });
                        }

                        return RedirectToAction("Index", "Courses", new { message = "Редагування розкладу успішно виконано." });
                    }
                    else
                    {
                        return RedirectToAction("EditSchedule", "Courses", new
                        {
                            error = await _coursesService.GetEditScheduleErrorMessage((KeyValuePair<int, int>)errors),
                            courseId = model.CourseId
                        });
                    }
                }
                catch
                {
                    // error
                    return RedirectToAction("Index", "Courses", new { error = "При редагуванні розкладу сталась помилка, бот недоступний!" });
                }
            }

            // error for Model
            else
            {
                ModelState.AddModelError(string.Empty, "Невідома помилка!");
            }
            return RedirectToAction("Index", "Courses");
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{courseId}/Students/{studentId}")]
        public async Task<IActionResult> RatingStudent(int courseId, int studentId)
        {
            Course course = await _coursesService.GetByIdAsync(courseId);

            try
            {
                if (course.HideMarks && User.IsInRole("Student"))
                {
                    return RedirectToAction("Index", "Courses");
                }
                else
                {
                    var model = await _marksServices.GetStudentModulesMarksAsync(courseId, studentId);

                    return View(model);
                }
            }
            catch
            {
                // not found 
                return RedirectToAction("Index", "Courses");
            }
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> Rating(int courseId)
        {
            var model = await _marksServices.GetRatingAsync(courseId);

            if (model.Course.HideMarks && User.IsInRole("Student"))
            {
                return RedirectToAction("Index", "Courses");
            }
            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}/Modules/{moduleId}")]
        public async Task<IActionResult> SetModuleMark(int courseId, int moduleId)
        {
            CourseModuleMarksViewModel model = await _marksServices.GetModuleStudentsMarksAsync(courseId, moduleId);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}/Modules/{moduleId}")]
        public async Task<IActionResult> SetModuleMark(CourseModuleMarksViewModel model)
        {

            try
            {
                var result = await _marksServices.UpdateModuleStudentsMarksAsync(model);

                if (result)
                {
                    // success
                    return RedirectToAction("Details", "Courses", new { courseId = model.Course.CourseId });
                }
                return RedirectToAction("Index", "Courses", new { error = "При виставленні оцінки за модуль сталася невідома помилка!" });
            }
            catch
            {
                // Error
                return RedirectToAction("Index", "Courses", new { error = "При виставленні оцінки за модуль сталася невідома помилка!" });
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}/Students/{studentId}")]
        public async Task<IActionResult> SetStudentMark(int courseId, int studentId)
        {
            CourseStudentMarksViewModel model = await _marksServices.GetStudentModulesMarksAsync(courseId, studentId);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}/Students/{studentId}")]
        public async Task<IActionResult> SetStudentMark(CourseStudentMarksViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _marksServices.UpdateStudentModulesMarksAsync(model);

                    if (result)
                    {
                        // success
                        return RedirectToAction("Details", "Courses", new { courseId = model.Course.CourseId });
                    }
                }
                catch
                {
                    // Error
                    return RedirectToAction("Index", "Courses");
                }

            }
            // Error for Model (Model.AddError or smth like that)
            return View(model);
        }
        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{courseId}")]
        public async Task<IActionResult> Delete(int courseId)
        {
            bool res = await _coursesService.DeleteAsync(courseId);
            if (res)
            {
                return Json(new { url = Url.Action("Index", "Courses", new { message = "Курс успішно видалено!" }) });
            }
            else
            {
                return Json(new { url = Url.Action("Index", "Courses", new { error = "Сталася невідома помилка при видаленні курсу!" }) });
            }

        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{markId}")]
        public async Task<IActionResult> MarkChat(int markId)
        {
            Mark res = await _marksServices.GetMarkByIdAsync(markId);
            if (res == null)
                return RedirectToAction("Index", "Courses", new { error = "Ви вибрали неіснуючу оцінку" });
            List<Comments> comments = (await _marksServices.GetCommentByMarkIdAsync(markId)).ToList();
            CourseStudentModuleChat courseStudentModuleChat = new CourseStudentModuleChat()
            {
                Mark = res,
                Comments = comments,
                MarkId = res.MarkId,
                UserId = (await _userManager.FindByNameAsync(User.Identity.Name)).Id
            };
            return View(courseStudentModuleChat);

        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{markId}")]
        public async Task<IActionResult> MarkChat(CourseStudentModuleChat model)
        {
            await _marksServices.InsertCommentAsync(new Comments
            {
                Comment = model.Comment,
                UserId = model.UserId,
                MarkId = model.MarkId
            });

            var user = await _userManager.GetUserAsync(User);
            var callbackUrl = Url.Action("MarkChat", "Courses", new { model.MarkId }, protocol: HttpContext.Request.Scheme);
            string emailAddress = "";
            string message = "";

            Mark mark = await _marksServices.GetMarkByIdAsync(model.MarkId);

            if (User.IsInRole("Teacher"))
            {
                emailAddress = (await _studentsService.GetByIdAsync(mark.StudentId)).User.Email;
            }
            else if (User.IsInRole("Student"))
            {
                emailAddress = (await _usersService.GetTeacherByMarkId(model.MarkId)).Email;
            }

            if (model.Comment != null)
            {
                message = model.Comment;
            }

            await _emailSender.SendMarkChatEmailAsync(emailAddress, "Нове повідомлення", message, $"{user.LastName} {user.FirstName} {user.MiddleName}", user.Email, mark.CourseModule.Course.Name, mark.CourseModule.Module.Name, mark.TestMark, mark.LabMark, callbackUrl);

            return RedirectToAction("MarkChat", new { markId = model.MarkId });

        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        [Route("{controller}/{action}/{courseId}/{moduleId}")]
        public async Task<IActionResult> ModuleRating(int courseId, int moduleId)
        {
            ModuleRatingViewModel model = await _marksServices.GetModuleRating(courseId, moduleId);

            if (model.Course.HideMarks && User.IsInRole("Student"))
            {
                return RedirectToAction("Index", "Courses");
            }

            return View(model);
        }
    }
}