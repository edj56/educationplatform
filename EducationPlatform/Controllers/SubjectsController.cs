﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using EducationPlatform.Models.ViewModels.Subjects;
using EducationPlatform.Models.ViewModels.Modules;

namespace EducationPlatform.Controllers
{
    public class SubjectsController : Controller
    {
        private readonly ISubjectsService _subjectService;
        private readonly IModulesService _modulesService;

        public SubjectsController(
            ISubjectsService subjectService,
            IModulesService modulesService
        )
        {
            _subjectService = subjectService;
            _modulesService = modulesService;
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Index(string error = null, string message = null)
        {
            ViewBag.Error = error;
            ViewBag.Message = message;

            return View(await _subjectService.GetSubjectsAsync());
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Teacher")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Create(SubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _subjectService.CreateAsync(model);
                return RedirectToAction("Index", "Subjects");
            }
            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Edit(int id)
        {
            return View("Edit", await _subjectService.GetByIdAsync(id));
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Edit(SubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _subjectService.UpdateAsync(model);
                return RedirectToAction("Index", "Subjects");
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _subjectService.DeleteAsync(id);
                return Json(new { url = Url.Action("Index", "Subjects", new { message = "Предмет успішно видалено!" }) });
            }
            catch
            {
                return Json(new { url = Url.Action("Index", "Subjects", new { error = "Даний предмет уже пов'язаний із курсом, спершу видаліть курс!" }) });
            }

        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{subjectId}/Modules")]
        public async Task<IActionResult> Modules(int subjectId, string error)
        {
            ViewBag.Error = error;
            ViewBag.SubjectId = subjectId;
            var subject = await _subjectService.GetByIdAsync(subjectId);
            ViewBag.Subject = subject.Name;
            List<ModuleViewModel> modules = (await _modulesService.GetModulesBySubjectId(subjectId)).ToList();
            return View(modules);
        }

        [HttpGet]
        [Route("{controller}/{action}/{moduleId}")]
        public async Task<IActionResult> ModuleFiles(int moduleId)
        {
            var module = await _modulesService.GetById(moduleId);

            if (module == null)
            {
                return RedirectToAction("Index", "Subjects");
            }

            var model = await _modulesService.GetModuleFiles(moduleId);

            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{subjectId}")]
        public async Task<IActionResult> CreateModule(int subjectId)
        {
            SubjectViewModel subject = await _subjectService.GetByIdAsync(subjectId);

            if (subject != null)
            {
                ViewBag.SubjectId = subjectId;
                return View();
            }

            return RedirectToAction("Index", "Subjects", new { error = "Предмет не знайдено" });
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{subjectId}")]
        public async Task<IActionResult> CreateModule(ModuleViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _modulesService.Insert(model);
                return RedirectToAction("Modules", "Subjects", new { subjectId = model.SubjectId });
            }
            return View(model);
        }

        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{moduleId}")]
        public async Task<IActionResult> EditModule(int moduleId)
        {
            ModuleViewModel module = await _modulesService.GetById(moduleId);
            return View(module);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{moduleId}")]
        public async Task<IActionResult> EditModule(ModuleViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _modulesService.Update(model);               
                return RedirectToAction("Modules", "Subjects", new { subjectId = model.Subject.SubjectId });
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Teacher")]
        [Route("{controller}/{action}/{moduleId}")]
        public async Task<IActionResult> DeleteModule(int moduleId, int subjectId)
        {
            try
            {
                await _modulesService.Delete(moduleId);
                return Json(new { url = Url.Action("Modules", "Subjects", new { subjectId }) });
            }
            catch
            {
                return Json(new { url = Url.Action("Modules", "Subjects", new { subjectId, error = "Ви не можете видалити цей модуль, оскільки він задіяний у курсі!" }) });
            }
        }
    }
}