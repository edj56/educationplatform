﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IVideosRepository
    {
        Task InsertVideoAsync(Video video);
        Task InsertCourseModuleVideoAsync(CourseModuleVideo courseModuleVideo);
        Task UpdateVideoAsync(int id, string link);
        Task DeleteVideoAsync(int videoId, int courseModuleId);
        Task DeleteVideoAsync(Video video);
        Task DeleteCourseModuleVideoAsync(CourseModuleVideo courseModuleVideo);
        Task<IEnumerable<SelectListItem>> GetCoursesForDropdownAsync();
        Task<Video> GetVideoByLinkAsync(string link);
        Task<Video> GetVideoByIdAsync(int id);
        Task<CourseModuleVideo> GetCourseModuleVideoAsync(int videoId, int courseModuleId);
        Task<IEnumerable<SelectListItem>> GetCourseModulesForDropdownAsync(int id);
    }
}
