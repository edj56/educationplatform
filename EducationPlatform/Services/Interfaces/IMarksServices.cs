﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Marks;
using EducationPlatform.Models.ViewModels.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IMarksServices
    {
        Task AddMarkAsync(Mark mark);
        Task<Mark> GetMarkByIdAsync(int markId);


        Task PlaceTestMarkAsync(Mark testMark, int mark);
        Task PlaceLabMarkAsync(Mark labMark, int mark);


        Task<IEnumerable<Mark>> GetMarksByStudentIdAndModuleIdAsync(int studentId, int moduleId);

        Task<List<MarkViewModel>> GetMarksViewModelsByStudentIdAndModuleIdAsync(int studentId, int moduleId);


        Task InsertCommentAsync(Comments comment);
        Task<IEnumerable<Comments>> GetCommentByMarkIdAsync(int markId);


        Task<CourseModuleMarksViewModel> GetModuleStudentsMarksAsync(int courseId, int moduleId);
        Task<bool> UpdateModuleStudentsMarksAsync(CourseModuleMarksViewModel model);
        Task<bool> UpdateStudentModulesMarksAsync(CourseStudentMarksViewModel model);
        Task<CourseStudentMarksViewModel> GetStudentModulesMarksAsync(int courseId, int studentId);
        Task<CourseRatingViewModel> GetRatingAsync(int courseId);
        Task<ModuleRatingViewModel> GetModuleRating(int courseId, int moduleId);
    }
}
