﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Schedule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IScheduleService
    {
        Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByStudent(string Id);
        Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByTeacher(string email);
        Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByAllCourses();
        Task<IEnumerable<ScheduleItemViewModel>> GetCoursesSchedulebyFilter(List<int> coursesId);
        Task<IEnumerable<ScheduleItemViewModel>> GetCoursesScheduleByTeacherFilter(List<string> email);
    }
}
