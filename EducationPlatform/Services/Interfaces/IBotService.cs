﻿using EducationPlatform.Models.ViewModels.Bot;
using EducationPlatform.Models.ViewModels.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IBotService
    {
        Task<PushNotificationsViewModel> PrepareForPushNotifications(CourseEditSchedulesViewModel model);
        Task SendPushNotificationsAsync(PushNotificationsViewModel model);
        Task<BotAuthorizationKeysViewModel> GetUserByKey(string key);
        Task<BotAuthorizedUsersViewModel> GetBotAuthorizedUser(string userId);
        Task CreateBotAuthorizedUser(string channelId, string userId, string channelUserId, string conversationId);
        Task DeleteBotAuthorizedUser(BotAuthorizedUsersViewModel botAuthorizedUser);
        Task<bool> CheckBotUserAuthorization(string channelUserId);
        Task<string> GetAuthorizedUserId(string channelUserId);
        Task<BotAuthorizationKeysViewModel> GetAuthorizationKeyByKey(string key);
        Task DeleteAuthorizationKey(BotAuthorizationKeysViewModel botKey);
        Task<BotAuthorizationKeysViewModel> GetAuthorizationKey(string id);
        Task CreateAuthorizationKey(string id);
    }
}
