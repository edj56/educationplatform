﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IUsersService
    {
        Task<bool> RegisterUserAsync(UserViewModel model, string role);
        Task<bool> RegisterStudentAsync(UserViewModel user, StudentViewModel model);
        Task<bool> RegisterTeacherAsync(UserViewModel model);
        Task<bool> UpdateAsync(UserViewModel model);
        Task<bool> BlockUser(string id);
        Task<bool> UnBlockUser(string id);
        Task ChangeEmail(User user, string newEmail);
        Task<bool> DeleteUserByEmailAsync(string email);
        Task<TwoFactorUserViewModel> GetTwoFactorUserByIdAsync(string id);
        Task UpdateTwoFactorUserAsync(TwoFactorUserViewModel user);
        Task DeleteTwoFactorUserAsync(TwoFactorUserViewModel model);
        Task AddTwoFactorUserAsync(string userId);
        Task<AspNetUserViewModel> GetAspNetUserByEmailAsync(string email);
        Task<AspNetUserLoginViewModel> GetAspNetUserLoginsByUserId(string userId);
        Task AddUserLoginsAsync(AspNetUserLoginViewModel userLogins);
        Task<IEnumerable<AspNetUsers>> GetTeachers();
        Task<AspNetUsers> GetByIdAsync(string id);
        Task<IEnumerable<AspNetUsers>> GetNotBannedUsersInRole(string roleName);
        Task<string> GetUserRoleAsync(string userId);
        Task<AspNetUsers> GetTeacherByMarkId(int markId);
    }
}
