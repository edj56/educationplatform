﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Subjects;
using Microsoft.AspNetCore.Http;

namespace EducationPlatform.Services.Interfaces
{
    public interface ISubjectsRepository
    {
        Task CreateAsync(Subject subject);
        Task UpdateAsync(Subject subject);
        Task DeleteAsync(Subject subject);
        Task<Subject> GetByIdAsync(int id);
        Task<Subject> GetWithModulesByIdAsync(int id);
        Task<IEnumerable<Subject>> GetSubjectsWithModulesAsync();
    }
}
