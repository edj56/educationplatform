﻿using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IBotRepository
    {
        Task<BotAuthorizationKeys> GetUserByKey(string key);
        Task<BotAuthorizedUsers> GetBotAuthorizedUser(string userId);
        Task CreateBotAuthorizedUser(string channelId, string userId, string channelUserId, string conversationId);
        Task DeleteBotAuthorizedUser(BotAuthorizedUsers botAuthorizedUser);
        Task<bool> CheckUserBotChannelId(string userId, string channelId);
        List<MessengersBots> GetMessangersBots();
        Task CreateAuthorizationKey(BotAuthorizationKeys key);
        Task<BotAuthorizationKeys> GetAuthorizationKey(string userId);
        Task<BotAuthorizationKeys> GetAuthorizationKeyByKey(string key);
        Task<string> GetAuthorizedUserId(string channelUserId);
        Task DeleteAuthorizationKey(BotAuthorizationKeys key);
        Task<bool> CheckBotUserAuthorization(string channelUserId);
    }
}
