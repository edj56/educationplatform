﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IEmailSender
    {
        Task SendDefaultEmailAsync(string email, string subject, string message, string buttonLink, string buttonText);
        Task SendMarkChatEmailAsync(string email, string subject, string message, string userFromFullName, string emailFrom, string courseName, string moduleName, int? testMark, int? labMark, string buttonLink);
    }
}
