﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Modules;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IModulesRepository
    {
        Task Insert(Module model);
        Task<Module> GetById(int id);
        Task Update(Module model);
        Task<IEnumerable<Module>> GetModulesBySubjectId(int id);
        Task Delete(Module module);
        Task DeleteRangeAsync(ICollection<Module> modules);
        // Task<bool> SaveFilesToModules(List<IFormFile> files, int moduleId);
        // Task<bool> RemoveModuleFiles(IEnumerable<ModuleFile> files);
        Task<List<ModuleFile>> GetModuleFiles(int moduleId);
        Task<List<CourseModule>> GetAllNextCourseModules();
        Task<List<CourseModule>> GetAllNextCourseModulesAvaliableForStudent(string channelId);
        Task<Module> GetModuleByIdAsync(int moduleId);
    }
}
