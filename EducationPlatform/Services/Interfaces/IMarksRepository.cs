﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IMarksRepository
    {
        void UpdateMark(Mark mark);
        Task<bool> SaveAsync();
        Task AddMarkAsync(Mark mark);
        Task<Mark> GetMarkByIdAsync(int markId);


        Task PlaceTestMarkAsync(Mark testMark, int mark);
        Task PlaceLabMarkAsync(Mark labMark, int mark);


        Task<IEnumerable<Mark>> GetMarksByCourseModuleIdAsync(params int[] courseModuleId);
        Task<IEnumerable<CourseStudent>> GetCourseStudentsByCourseIdAsync(int courseId);
        Task<IEnumerable<Mark>> GetMarksByStudentIdAndModuleIdAsync(int studentId, int moduleId);



        Task InsertCommentAsync(Comments comment);
        Task<IEnumerable<Comments>> GetCommentByMarkIdAsync(int markId);
    }
}