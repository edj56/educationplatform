﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface ICoursesRepository
    {
        Task InsertAsync(Course model);
        Task<Course> GetByIdAsync(int id);
        Task UpdateAsync(Course model);
        Task<IEnumerable<Course>> GetCoursesAsync();
        Task<bool> DeleteAsync(int id);
        Task<Course> GetFirstCourseAsync();
        Task<bool> SaveAsync();


        Task<IEnumerable<CourseModule>> GetCourseModulesAsync();
        Task<IEnumerable<Module>> GetModulesByCourseIdAsync(int courseId);
        Task RemoveModulesFromCourseAsync(List<int> notSelectedModulesId);
        Task IncludeNewModulesAsync(List<int> newModulesId, int courseId);
        Task RemoveStudentsFromCourseAsync(List<int> notSelectedStudents, int courseId);
        Task IncludeNewStudentsAsync(List<int> newStudentsId, int courseId);
        Task<IEnumerable<Student>> GetStudentsByCourseIdAsync(int courseId);
        Task<bool> RemoveModulesFromCourseAsync(int courseId);
        Task<IEnumerable<CourseModule>> GetCourseModulesByCourseIdAsync(int courseId);
        Task<IEnumerable<CourseModule>> GetCourseModulesAsync(List<int> modulesId, int courseId);
        Task<bool> UpdateCourseModulesAsync(List<CourseModule> courseModule);



        Task<AspNetUsers> GetCourseTeacherAsync(int courseId);
        Task<IEnumerable<CourseStudent>> GetCourseStudentsAsync(int courseId);
        Task<List<Course>> GetStudentCoursesAsync(int studentId);
        Task<IEnumerable<Course>> GetTeacherCoursesAsync(string userId);
        Task<IEnumerable<Course>> GetCoursesRangeAsync(IEnumerable<int> ids);
        Task<CourseModule> GetCourseModuleByIdAsync(int id);
    }
}
