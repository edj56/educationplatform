﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Students;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IUsersRepository
    {
        Task<bool> AddUserRoleAsync(AspNetUserRoles userRole);
        Task<AspNetRoles> FindRoleByNameAsync(string name);
        Task<List<AspNetUserRoles>> GetUserRolesByIdAsync(string id);
        Task RemoveUserRolesAsync(List<AspNetUserRoles> userRoles);
        Task<bool> CheckIfUserIsStudentByIdAsync(string id);
        Task<bool> CreateStudentAsync(Student model);
        Task AddUserLoginsAsync(AspNetUserLogins userLogins);
        Task<AspNetUserLogins> GetAspNetUserLoginsByUserId(string userId);
        Task<bool> UpdateAsync(AspNetUsers user);
        Task AddTwoFactorUserAsync(TwoFactorUser user);
        Task UpdateTwoFactorUserAsync(TwoFactorUser user);
        Task DeleteTwoFactorUserAsync(TwoFactorUser user);
        Task<TwoFactorUser> GetTwoFactorUserByIdAsync(string id);
        Task<IEnumerable<AspNetUsers>> GetTeachers();
        Task<IEnumerable<AspNetUsers>> GetNotBannedUsersInRole(string roleName);
        Task<string> GetUserRoleAsync(string userId);
        Task<AspNetUsers> GetTeacherByMarkId(int markId);
        Task<AspNetUsers> GetUserByIdAsync(string id);
        Task<AspNetUsers> GetUserByEmailAsync(string email);
    }
}
