﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface ICoursesService
    {
        Task InsertAsync(Course model);
        Task<Course> GetByIdAsync(int id);
        Task UpdateAsync(Course model);
        Task<IEnumerable<Course>> GetCoursesAsync();
        Task<bool> DeleteAsync(int id);
        Task<Course> GetFirstCourseAsync();

        Task<CoursesIndexViewModel> GetStudentCoursesByIdAsync(int studentId);
        Task<IEnumerable<Module>> GetModulesByCourseIdAsync(int courseId);
        Task<IEnumerable<Student>> GetStudentsByCourseIdAsync(int courseId);
        Task<bool> RemoveModulesFromCourseAsync(int courseId);

        Task<CoursesIndexViewModel> GetTeacherCoursesByIdAsync(string teacherId);

        Task<AspNetUsers> GetCourseTeacherAsync(int courseId);
        Task<List<CourseStudentViewModel>> GetCourseStudentsAsync(int courseId);
        Task<List<CourseViewModel>> GetTeacherCoursesAsync(string userId);



        Task<bool> EditModulesAsync(CourseEditModulesViewModel model);
        Task<bool> EditStudentsAsync(CourseEditStudentsViewModel model);
        Task<IEnumerable<CourseModuleEditViewModel>> GetScheduleModulesAsync(int courseId);
        Task<IEnumerable<CourseDetailsModuleViewModel>> GetScheduleModulesForDetailsAsync(int courseId);
        Task<KeyValuePair<int, int>?> EditSchedulesAsync(CourseEditSchedulesViewModel model);
        Task<IEnumerable<CourseModule>> GetChangedCoursesAsync(CourseEditSchedulesViewModel model);
        Task<string> GetEditScheduleErrorMessage(KeyValuePair<int, int> error);
    }
}
