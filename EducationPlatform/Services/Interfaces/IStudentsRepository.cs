﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IStudentsRepository
    {
        Task<bool> HasStudentAccessAsync(string email);
        Task<bool> UpdateAsync(Student student);
        Task<bool> DeleteAsync(Student student);
        Task<Student> GetByIdAsync(int id);
        Task<Student> GetByEmailAsync(string email);
        Task<Student> GetByUserIdAsync(string id);
        Task<List<Student>> GetActiveNonBannedStudentsAsync();
        Task<List<Student>> GetActiveStudentsAsync();
        Task<List<Student>> GetEntrantsAsync();
        Task<List<Student>> GetStudentsByCourseIdAsync(int id);
        Task<List<CourseStudent>> GetCourseStudentByIdAsync(int id);
        Task<List<Course>> GetStudentCoursesAsync(int id);
    }
}
