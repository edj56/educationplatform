﻿using EducationPlatform.Models.ViewModels.Subjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface ISubjectsService
    {
        Task CreateAsync(SubjectViewModel model);
        Task UpdateAsync(SubjectViewModel model);
        Task DeleteAsync(int id);
        Task<SubjectViewModel> GetByIdAsync(int id);
        Task<IEnumerable<SubjectViewModel>> GetSubjectsAsync();
    }
}
