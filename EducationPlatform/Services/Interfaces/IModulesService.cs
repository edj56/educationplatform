﻿using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Modules;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IModulesService
    {
        Task Insert(ModuleViewModel model);
        Task<ModuleViewModel> GetById(int id);
        Task Update(ModuleViewModel model);
        Task<IEnumerable<ModuleViewModel>> GetModulesBySubjectId(int id);
        Task<ModuleFileListViewModel> GetModuleFiles(int id);
        Task Delete(int id);
        Task<List<CourseModuleViewModel>> GetAllNextCourseModules();
        Task<List<CourseModuleViewModel>> GetAllNextCourseModulesAvaliableForStudent(string channelId);
    }
}
