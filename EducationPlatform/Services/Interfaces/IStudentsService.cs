﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IStudentsService
    {
        Task<bool> UpdateAsync(StudentFormViewModel model, int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> ChangeAccessAsync(int id, bool access);
        Task<IEnumerable<StudentViewModel>> GetActiveStudentsAsync();
        Task<StudentViewModel> GetByUserIdAsync(string id);
        Task<StudentViewModel> GetByIdAsync(int id);
        Task<List<CourseViewModel>> GetStudentCoursesAsync(int id);
        Task<IEnumerable<StudentViewModel>> GetActiveNonBannedStudentsAsync();
        Task<IEnumerable<StudentViewModel>> GetEntrantsAsync();
    }
}
