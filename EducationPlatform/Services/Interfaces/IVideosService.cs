﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Videos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public interface IVideosService
    {
        Task<VideoViewModel> GetVideoViewModelAsync();
        Task<VideoViewModel> GetVideoViewModelAsync(int videoId, int courseModuleId);
        Task AddVideoAsync(VideoViewModel model);
        Task EditVideoAsync(VideoViewModel model);
        Task DeleteVideoAsync(int videoId, int courseModuleId);
        Task<JsonResult> GetModulesForDdropdownAsync(string id);
        Task<IEnumerable<Course>> GetCourses();
    }
}
