﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class CoursesService : ICoursesService
    {
        private readonly ICoursesRepository _coursesRepository;
        private readonly IModulesRepository _modulesRepository;

        public CoursesService
        (
            ICoursesRepository coursesRepository,
            IModulesRepository modulesRepository
        )
        {
            _coursesRepository = coursesRepository;
            _modulesRepository = modulesRepository;
        }


        public async Task InsertAsync(Course model)
        {
            await _coursesRepository.InsertAsync(model);
        }
        public async Task<Course> GetByIdAsync(int id)
        {
            return await _coursesRepository.GetByIdAsync(id);
        }
        public async Task UpdateAsync(Course model)
        {
            await _coursesRepository.UpdateAsync(model);
        }
        public async Task<IEnumerable<Course>> GetCoursesAsync()
        {
            return await _coursesRepository.GetCoursesAsync();
        }
        public async Task<bool> DeleteAsync(int id)
        {
            return await _coursesRepository.DeleteAsync(id);
        }
        public async Task<Course> GetFirstCourseAsync()
        {
            return await _coursesRepository.GetFirstCourseAsync();
        }

        public async Task<IEnumerable<Module>> GetModulesByCourseIdAsync(int courseId)
        {
            return await _coursesRepository.GetModulesByCourseIdAsync(courseId);
        }
        public async Task<IEnumerable<Student>> GetStudentsByCourseIdAsync(int courseId)
        {
            return await _coursesRepository.GetStudentsByCourseIdAsync(courseId);
        }
        public async Task<bool> RemoveModulesFromCourseAsync(int courseId)
        {
            return await _coursesRepository.RemoveModulesFromCourseAsync(courseId);
        }

        public async Task<CoursesIndexViewModel> GetStudentCoursesByIdAsync(int studentId)
        {
            return new CoursesIndexViewModel
            {
                Courses = await _coursesRepository.GetStudentCoursesAsync(studentId)
            };
        }

        public async Task<AspNetUsers> GetCourseTeacherAsync(int courseId)
        {
            return await _coursesRepository.GetCourseTeacherAsync(courseId);
        }
        public async Task<List<CourseStudentViewModel>> GetCourseStudentsAsync(int courseId)
        {
            List<CourseStudentViewModel> courseStudents = new List<CourseStudentViewModel>();
            foreach (var courseStudent in await _coursesRepository.GetCourseStudentsAsync(courseId))
            {
                courseStudents.Add(new CourseStudentViewModel(courseStudent));
            }
            return courseStudents;
        }
        public async Task<List<CourseViewModel>> GetTeacherCoursesAsync(string userId)
        {
            List<CourseViewModel> courses = new List<CourseViewModel>();
            foreach (var course in await _coursesRepository.GetTeacherCoursesAsync(userId))
            {
                courses.Add(new CourseViewModel(course));
            }
            return courses;
        }



        public async Task<bool> EditModulesAsync(CourseEditModulesViewModel model)
        {
            var notSelectedModules = model.Modules.Where(x => !x.Selected).Select(x => x.Id).ToList();
            await _coursesRepository.RemoveModulesFromCourseAsync(notSelectedModules);


            var newModules = model.Modules.Where(x => x.Selected).Select(x => x.Id).ToList();
            await _coursesRepository.IncludeNewModulesAsync(newModules, model.CourseId);


            return await _coursesRepository.SaveAsync();
        }
        public async Task<bool> EditStudentsAsync(CourseEditStudentsViewModel model)
        {
            var notSelectedStudents = model.Students.Where(x => !x.Selected).Select(x => x.Id).ToList();
            await _coursesRepository.RemoveStudentsFromCourseAsync(notSelectedStudents, model.CourseId);



            var newStudents = model.Students.Where(x => x.Selected).Select(x => x.Id).ToList();
            await _coursesRepository.IncludeNewStudentsAsync(newStudents, model.CourseId);


            return await _coursesRepository.SaveAsync();
        }
        public async Task<IEnumerable<CourseModuleEditViewModel>> GetScheduleModulesAsync(int courseId)
        {
            var courseModules = await _coursesRepository.GetCourseModulesByCourseIdAsync(courseId);
            var courseModuleEditViewModels = courseModules.Select(x => new CourseModuleEditViewModel
            {
                Id = x.ModuleId,
                Name = x.Module.Name,
                Description = x.Module.Description,
                Date = x.Date.HasValue ? x.Date.Value : DateTime.MinValue,
                StartTime = x.StartTime.HasValue ? x.StartTime.Value : TimeSpan.Zero,
                Duration = x.Module.Duration.HasValue ? x.Module.Duration : TimeSpan.FromHours(2)                // Якщо тривалість не вказана то значення по замовчуванню 2 години як і у розкладі !!!)
            }).ToList();
            return courseModuleEditViewModels;
        }
        public async Task<IEnumerable<CourseDetailsModuleViewModel>> GetScheduleModulesForDetailsAsync(int courseId)
        {
            var courseModules = await _coursesRepository.GetCourseModulesByCourseIdAsync(courseId);
            var courseModuleEditViewModels = courseModules.Select(x => new CourseDetailsModuleViewModel
            {
                Module = x.Module,
                Name = x.Module.Name,
                Description = x.Module.Description,
                Date = x.Date.HasValue ? x.Date.Value : DateTime.MinValue,
                StartTime = x.StartTime.HasValue ? x.StartTime.Value : TimeSpan.Zero
            }).ToList();
            return courseModuleEditViewModels;
        }
        public async Task<KeyValuePair<int, int>?> EditSchedulesAsync(CourseEditSchedulesViewModel model)
        {
            var checkingResult = await CheckForCoursesIntersections(model);

            if (checkingResult != null)
            {
                return checkingResult;
            }

            var courseModulesId = model.Modules.Select(x => x.Id).ToList();
            var courseModule = (await _coursesRepository.GetCourseModulesAsync(courseModulesId, model.CourseId)).ToList();

            courseModule.ForEach(x =>
            {
                x.Date = model.Modules.First(y => y.Id == x.ModuleId).Date;
                x.StartTime = model.Modules.First(y => y.Id == x.ModuleId).StartTime;
            });

            await _coursesRepository.UpdateCourseModulesAsync(courseModule);

            return null;
        }
        public async Task<IEnumerable<CourseModule>> GetChangedCoursesAsync(CourseEditSchedulesViewModel model)
        {
            List<CourseModule> changedCourseModules = new List<CourseModule>();
            IEnumerable<CourseModule> courseModules = await _coursesRepository.GetCourseModulesAsync();

            foreach (CourseModuleEditViewModel courseModule in model.Modules)
            {
                changedCourseModules.Add(courseModules.SingleOrDefault(cm => cm.CourseModuleId == courseModule.Id));
            }

            return changedCourseModules;
        }

        public async Task<CoursesIndexViewModel> GetTeacherCoursesByIdAsync(string teacherId)
        {
            return new CoursesIndexViewModel
            {
                Courses = (await _coursesRepository.GetTeacherCoursesAsync(teacherId)).ToList()
            };
        }


        public async Task<string> GetEditScheduleErrorMessage(KeyValuePair<int, int> error)
        {
            CourseModule courseModule = await _coursesRepository.GetCourseModuleByIdAsync(error.Value);
            return $"Модуль {(await _modulesRepository.GetModuleByIdAsync(error.Key)).Name} перетинається з модулем {courseModule.Module.Name} курсу {courseModule.Course.Name}";
        }

        private async Task<KeyValuePair<int, int>?> CheckForCoursesIntersections(CourseEditSchedulesViewModel courseEditingModel)
        {
            List<Student> students = (await _coursesRepository.GetStudentsByCourseIdAsync(courseEditingModel.CourseId)).ToList();
            HashSet<int> hashSet = new HashSet<int>();

            foreach(Student student in students)
            {
                hashSet.UnionWith(student.CourseStudent.Select(s => (int)s.CourseId));
            }

            List<Course> courses = (await _coursesRepository.GetCoursesRangeAsync(hashSet.ToList())).ToList();

            foreach(Course course in courses)
            {
                foreach(CourseModuleEditViewModel editingModule in courseEditingModel.Modules)
                {
                    DateTime editingModuleStart = editingModule.Date.Add((TimeSpan)editingModule.StartTime);
                    DateTime editingModuleEnd = editingModuleStart.Add((TimeSpan)editingModule.Duration);
                    
                    foreach(CourseModule courseModule in course.CourseModule)
                    {
                        if(courseModule.Date == null || courseModule.ModuleId == editingModule.Id)
                        {
                            continue;
                        }

                        DateTime courseModuleStart = ((DateTime)courseModule.Date).Add((TimeSpan)courseModule.StartTime);
                        DateTime courseModuleEnd = courseModuleStart.Add((TimeSpan)courseModule.Module.Duration);

                        if(DateTime.Compare(editingModuleEnd, courseModuleEnd) < 0)
                        {
                            if(DateTime.Compare(editingModuleStart, courseModuleStart) > 0 || DateTime.Compare(editingModuleEnd, courseModuleStart) > 0)
                            {
                                return new KeyValuePair<int, int>(editingModule.Id, courseModule.CourseModuleId);
                            }
                        }
                        else
                        {
                            if(DateTime.Compare(courseModuleStart, editingModuleStart) > 0 || DateTime.Compare(courseModuleEnd, editingModuleStart) > 0)
                            {
                                return new KeyValuePair<int, int>(editingModule.Id, courseModule.CourseModuleId);
                            }
                        }
                    }
                }

            }

            return null;
        }
    }
}
