﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Modules;
using EducationPlatform.Services.Interfaces;
using EducationPlatform.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class ModulesService : IModulesService
    {
        private readonly IModulesRepository _modulesRepository;

        public ModulesService(IModulesRepository modulesRepository)
        {
            _modulesRepository = modulesRepository;
        }

        public async Task Insert(ModuleViewModel model)
        {
            await _modulesRepository.Insert(new Module
            {
                SubjectId = model.SubjectId,
                Name = model.Name,
                Description = model.Description,
                Duration = model.Duration,
                HasTest = model.HasTest,
                HasLab = model.HasLab,
                MinTestMark = model.MinTestMark,
                MaxTestMark = model.MaxTestMark,
                MinLabMark = model.MinLabMark,
                MaxLabMark = model.MaxLabMark,
            });
        }

        public async Task<ModuleViewModel> GetById(int id)
        {
            return new ModuleViewModel(await _modulesRepository.GetById(id));
        }

        public async Task Update(ModuleViewModel model)
        {
            Module module = await _modulesRepository.GetById(model.ModuleId);

            if (module != null)
            {
                module.Name = model.Name;
                module.Description = model.Description;
                module.Duration = model.Duration;
                module.HasTest = model.HasTest;
                module.HasLab = model.HasLab;
                module.MinTestMark = model.MinTestMark;
                module.MaxTestMark = model.MaxTestMark;
                module.MinLabMark = model.MinLabMark;
                module.MaxLabMark = model.MaxLabMark;

                await _modulesRepository.Update(module);
            }
        }

        public async Task<IEnumerable<ModuleViewModel>> GetModulesBySubjectId(int id)
        {
            return (await _modulesRepository.GetModulesBySubjectId(id)).Select(m => new ModuleViewModel(m));
        }

        public async Task<ModuleFileListViewModel> GetModuleFiles(int id)
        {
            return new ModuleFileListViewModel
            {
                Files = (await _modulesRepository.GetModuleFiles(id)).Select(x => new ModuleFileViewModel(x)).ToList(),
                Module = new ModuleViewModel(await _modulesRepository.GetById(id))
            };
        }

        public async Task Delete(int id)
        {
            Module module = await _modulesRepository.GetById(id);

            if (module != null)
            {
                await _modulesRepository.Delete(module);
            }
        }

        public async Task<List<CourseModuleViewModel>> GetAllNextCourseModules()
        {
            return (await _modulesRepository.GetAllNextCourseModules())
                .Select(cm => new CourseModuleViewModel(cm))
                .ToList();
        }

        public async Task<List<CourseModuleViewModel>> GetAllNextCourseModulesAvaliableForStudent(string channelId)
        {
            return (await _modulesRepository.GetAllNextCourseModulesAvaliableForStudent(channelId))
                .Select(cm => new CourseModuleViewModel(cm))
                .ToList();
        }
    }
}
