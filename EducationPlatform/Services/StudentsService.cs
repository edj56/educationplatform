﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class StudentsService : IStudentsService
    {
        private readonly IStudentsRepository _studentsRepository;

        public StudentsService(IStudentsRepository studentsRepository)
        {
            _studentsRepository = studentsRepository;
        }

        public async Task<bool> UpdateAsync(StudentFormViewModel model, int id)
        {
            Student student = await _studentsRepository.GetByIdAsync(id);

            if (student != null)
            {
                student.University = model.University;
                student.Faculty = model.Faculty;
                student.Skills = model.Skills;
                student.StudyYear = model.StudyYear;
                student.HasAccess = model.HasAccess;

                return await _studentsRepository.UpdateAsync(student);
            }

            return false;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            Student student = await _studentsRepository.GetByIdAsync(id);

            if (student != null)
            {
                return await _studentsRepository.DeleteAsync(student);
            }

            return false;
        }

        public async Task<bool> ChangeAccessAsync(int id, bool access)
        {
            var student = await _studentsRepository.GetByIdAsync(id);

            if (student != null)
            {
                student.HasAccess = access;

                return await _studentsRepository.UpdateAsync(student);
            }

            return false;
        }

        public async Task<IEnumerable<StudentViewModel>> GetActiveStudentsAsync()
        {
            return (await _studentsRepository.GetActiveStudentsAsync()).Select(student => new StudentViewModel(student));
        }

        public async Task<StudentViewModel> GetByUserIdAsync(string id)
        {
            return new StudentViewModel(await _studentsRepository.GetByUserIdAsync(id));
        }

        public async Task<StudentViewModel> GetByIdAsync(int id)
        {
            return new StudentViewModel(await _studentsRepository.GetByIdAsync(id));
        }

        public async Task<List<CourseViewModel>> GetStudentCoursesAsync(int id)
        {
            List<CourseViewModel> courses = new List<CourseViewModel>();
            foreach (var course in await _studentsRepository.GetStudentCoursesAsync(id))
            {
                courses.Add(new CourseViewModel(course));
            }
            return courses;
        }

        public async Task<IEnumerable<StudentViewModel>> GetActiveNonBannedStudentsAsync()
        {
            return (await _studentsRepository.GetActiveNonBannedStudentsAsync()).Select(student => new StudentViewModel(student));
        }

        public async Task<IEnumerable<StudentViewModel>> GetEntrantsAsync()
        {
            return (await _studentsRepository.GetEntrantsAsync()).Select(student => new StudentViewModel(student));
        }
    }
}
