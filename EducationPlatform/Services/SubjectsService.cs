﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Subjects;
using EducationPlatform.Services.Interfaces;

namespace EducationPlatform.Services
{
    public class SubjectsService : ISubjectsService
    {
        private readonly ISubjectsRepository _subjectsRepository;
        private readonly IModulesRepository _modulesRepository;

        public SubjectsService(ISubjectsRepository subjectsRepository, IModulesRepository modulesRepository)
        {
            _subjectsRepository = subjectsRepository;
            _modulesRepository = modulesRepository;
        }

        public async Task CreateAsync(SubjectViewModel model)
        {
            await _subjectsRepository.CreateAsync(new Subject
            {
                Name = model.Name,
                Description = model.Description
            });
        }

        public async Task UpdateAsync(SubjectViewModel model)
        {
            await _subjectsRepository.UpdateAsync(new Subject
            {
                SubjectId = model.SubjectId,
                Name = model.Name,
                Description = model.Description
            });
        }

        public async Task DeleteAsync(int id)
        {
            Subject subject = await _subjectsRepository.GetWithModulesByIdAsync(id);
            
            if (subject != null)
            {
                await _modulesRepository.DeleteRangeAsync(subject.Module);
                await _subjectsRepository.DeleteAsync(subject);
            }
        }

        public async Task<SubjectViewModel> GetByIdAsync(int id)
        {
            Subject subject = await _subjectsRepository.GetByIdAsync(id);

            if (subject != null)
            {
                return new SubjectViewModel(subject);
            }

            return null;
        }

        public async Task<IEnumerable<SubjectViewModel>> GetSubjectsAsync()
        {
            List<SubjectViewModel> subjects = new List<SubjectViewModel>();

            foreach (Subject subject in await _subjectsRepository.GetSubjectsWithModulesAsync())
            {
                subjects.Add(new SubjectViewModel
                {
                    SubjectId = subject.SubjectId,
                    Name = subject.Name,
                    Description = subject.Description,
                    Module = subject.Module
                });
            }
            return subjects;
        }
    }
}
