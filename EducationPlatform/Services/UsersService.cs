﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;

namespace EducationPlatform.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IEmailSender _emailSender;
        private readonly LinkGenerator _urlHelper;
        private readonly UserManager<User> _userManager;

        public UsersService
        (
            IUsersRepository usersRepository,
            UserManager<User> userManager,
            IHttpContextAccessor httpContext,
            LinkGenerator urlHelper,
            IEmailSender emailSender
        )
        {
            _userManager = userManager;
            _usersRepository = usersRepository;
            _httpContext = httpContext;
            _emailSender = emailSender;
            _urlHelper = urlHelper;
        }

        public async Task<bool> RegisterUserAsync(UserViewModel model, string role)
        {
            User user = new User
            {
                Email = model.Email,
                UserName = model.Email,
                LastName = model.LastName,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                PhoneNumber = model.PhoneNumber,
                IsBanned = model.IsBanned
            };

            IdentityResult resultCreate;

            if (model.Password != null)
            {
                resultCreate = await _userManager.CreateAsync(user, model.Password);
            }
            else
            {
                resultCreate = await _userManager.CreateAsync(user);
            }

            if (resultCreate.Succeeded)
            {


                await _usersRepository.AddUserRoleAsync(new AspNetUserRoles
                {
                    UserId = user.Id,
                    RoleId = (await _usersRepository.FindRoleByNameAsync(role)).Id
                });

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = _urlHelper.GetUriByAction(_httpContext.HttpContext, "ConfirmEmail", "Account", new { userId = user.Id, code, email = user.Email });

                await _emailSender.SendDefaultEmailAsync(user.Email, "Підтвердження аккаунту", $"Перейдіть за посиланням:", callbackUrl, "Активувати!");

                return true;
            }

            return false;
        }

        public async Task<bool> RegisterStudentAsync(UserViewModel user, StudentViewModel student)
        {
            await RegisterUserAsync(user, "Student");

            return await _usersRepository.CreateStudentAsync(new Student
            {
                User = await _usersRepository.GetUserByEmailAsync(user.Email),
                University = student.University,
                Faculty = student.Faculty,
                StudyYear = student.StudyYear,
                Skills = student.Skills,
                HasAccess = student.HasAccess
            });
        }

        public async Task<bool> RegisterTeacherAsync(UserViewModel model)
        {
            return await RegisterUserAsync(model, "Teacher");
        }

        public async Task<bool> UpdateAsync(UserViewModel model)
        {
            var user = await _usersRepository.GetUserByEmailAsync(model.Email);

            if (user != null)
            {
                user.LastName = model.LastName;
                user.FirstName = model.FirstName;
                user.MiddleName = model.MiddleName;
                user.PhoneNumber = model.PhoneNumber;

                return await _usersRepository.UpdateAsync(user);
            }

            return false;
        }

        public async Task<bool> BlockUser(string id)
        {
            return await SetBanStatus(true, id);
        }

        public async Task<bool> UnBlockUser(string id)
        {
            return await SetBanStatus(false, id);
        }

        private async Task<bool> SetBanStatus(bool value, string id)
        {
            var user = await _usersRepository.GetUserByIdAsync(id);

            if (user != null)
            {
                if (await _userManager.IsInRoleAsync(await _userManager.FindByIdAsync(id), "Admin")) return false;

                user.IsBanned = value;

                await _usersRepository.RemoveUserRolesAsync(await _usersRepository.GetUserRolesByIdAsync(user.Id));

                if (value)
                {

                    await _usersRepository.AddUserRoleAsync(new AspNetUserRoles
                    {
                        RoleId = (await _usersRepository.FindRoleByNameAsync("Banned")).Id,
                        UserId = id
                    });
                }
                else
                {
                    var isStudent = await _usersRepository.CheckIfUserIsStudentByIdAsync(id);
                    var roleName = isStudent ? "Student" : "Teacher";

                    await _usersRepository.AddUserRoleAsync(new AspNetUserRoles
                    {
                        RoleId = (await _usersRepository.FindRoleByNameAsync(roleName)).Id,
                        UserId = id
                    });

                }

                return await _usersRepository.UpdateAsync(user);
            }

            return false;
        }

        public async Task ChangeEmail(User user, string newEmail)
        {
            AspNetUsers aspNetUser = await _usersRepository.GetUserByEmailAsync(user.Email);
            aspNetUser.Email = newEmail;
            aspNetUser.NormalizedEmail = newEmail.ToUpper();
            aspNetUser.UserName = newEmail;
            aspNetUser.NormalizedUserName = newEmail.ToUpper();

            await _usersRepository.UpdateAsync(aspNetUser);
        }

        public async Task<bool> DeleteUserByEmailAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user != null)
            {
                await _userManager.DeleteAsync(user);
                return true;
            }
            return false;
        }

        public async Task<TwoFactorUserViewModel> GetTwoFactorUserByIdAsync(string id)
        {
            TwoFactorUser twoFactorUser = await _usersRepository.GetTwoFactorUserByIdAsync(id);

            if (twoFactorUser != null)
            {
                return new TwoFactorUserViewModel(twoFactorUser);
            }

            return null;
        }

        public async Task UpdateTwoFactorUserAsync(TwoFactorUserViewModel model)
        {
            TwoFactorUser twoFactorUser = await _usersRepository.GetTwoFactorUserByIdAsync(model.UserId);

            twoFactorUser.Code = model.Code;

            await _usersRepository.UpdateTwoFactorUserAsync(twoFactorUser);
        }

        public async Task DeleteTwoFactorUserAsync(TwoFactorUserViewModel model)
        {
            await _usersRepository.DeleteTwoFactorUserAsync(await _usersRepository.GetTwoFactorUserByIdAsync(model.UserId));
        }

        public async Task AddTwoFactorUserAsync(string userId)
        {
            await _usersRepository.AddTwoFactorUserAsync(new TwoFactorUser
            {
                UserId = userId
            });
        }

        public async Task<IEnumerable<AspNetUsers>> GetTeachers()
        {
            return await _usersRepository.GetTeachers();
        }

        public Task<AspNetUsers> GetByIdAsync(string id)
        {
            return _usersRepository.GetUserByIdAsync(id);
        }

        public async Task<AspNetUserViewModel> GetAspNetUserByEmailAsync(string id)
        {
            return new AspNetUserViewModel(await _usersRepository.GetUserByEmailAsync(id));
        }

        public async Task<AspNetUserLoginViewModel> GetAspNetUserLoginsByUserId(string userId)
        {
            return new AspNetUserLoginViewModel(await _usersRepository.GetAspNetUserLoginsByUserId(userId));
        }

        public async Task AddUserLoginsAsync(AspNetUserLoginViewModel model)
        {
            await _usersRepository.AddUserLoginsAsync(new AspNetUserLogins
            {
                LoginProvider = model.LoginProvider,
                UserId = model.UserId,
                ProviderKey = model.ProviderKey,
                ProviderDisplayName = model.ProviderDisplayName
            });
        }

        public async Task<IEnumerable<AspNetUsers>> GetNotBannedUsersInRole(string roleName)
        {
            return await _usersRepository.GetNotBannedUsersInRole(roleName);
        }

        public async Task<string> GetUserRoleAsync(string userId)
        {
            return await _usersRepository.GetUserRoleAsync(userId);
        }

        public async Task<AspNetUsers> GetTeacherByMarkId(int markId)
        {
            return await _usersRepository.GetTeacherByMarkId(markId);
        }
    }
}