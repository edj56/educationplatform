using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class CoursesRepository : ICoursesRepository
    {
        private readonly EducationPlatformContext _context;
        public CoursesRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task InsertAsync(Course model)
        {
            _context.Course.Add(model);
            await _context.SaveChangesAsync();
        }
        public async Task<Course> GetByIdAsync(int id)
        {
            return await _context.Course
                .Include(x => x.Subject)
                .Include(x => x.Teacher)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.Module)
                .Include(x => x.CourseStudent)
                .Where(x => x.CourseId == id)
                .FirstOrDefaultAsync();
        }
        public async Task UpdateAsync(Course model)
        {
            _context.Course.Update(model);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Course>> GetCoursesAsync()
        {
            return await _context.Course
                .Include(x => x.Subject)
                .Include(x => x.Teacher)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.Module)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.CourseModuleVideo)
                .ThenInclude(x => x.Video)
                .Include(x => x.CourseStudent)
                .ThenInclude(x => x.Student)
                .ThenInclude(x => x.User)
                .ToListAsync();
        }
        public async Task<List<Course>> GetStudentCoursesAsync(int studentId)

        {
            return await _context.CourseStudent
               .Where(x => x.StudentId == studentId)
               .Include(x => x.Course)
               .Select(x => x.Course)
               .Include(x => x.Subject)
               .Include(x => x.Teacher)
               .Include(x => x.CourseModule)
               .ThenInclude(x => x.Module)
               .Include(x => x.CourseModule)
               .ThenInclude(x => x.CourseModuleVideo)
               .ThenInclude(x => x.Video)
               .Include(x => x.CourseStudent)
               .ThenInclude(x => x.Student)
               .ThenInclude(x => x.User)
               .ToListAsync();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            Course courseFromDB = _context.Course.Where(x => x.CourseId == id).FirstOrDefault();
            List<CourseModule> courseModulesFromDb = _context.CourseModule.Where(x => x.CourseId == courseFromDB.CourseId).ToList();
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    foreach (CourseModule courseModule in courseModulesFromDb)
                    {
                        _context.Mark.RemoveRange(_context.Mark.Where(x => x.CourseModuleId == courseModule.CourseModuleId));
                    }
                    _context.CourseModule.RemoveRange(_context.CourseModule.Where(x => x.CourseId == courseFromDB.CourseId));
                    _context.CourseStudent.RemoveRange(_context.CourseStudent.Where(x => x.CourseId == courseFromDB.CourseId));
                    _context.Course.Remove(courseFromDB);
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return false;
                }
            }
            return true;
        }
        public async Task<Course> GetFirstCourseAsync()
        {
            return await _context.Course.FirstOrDefaultAsync();
        }
        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() >= 0;
        }



        public async Task<IEnumerable<CourseModule>> GetCourseModulesAsync()
        {
            return await _context.CourseModule.Include(m => m.Module).ToListAsync();
        }
        public async Task<IEnumerable<Module>> GetModulesByCourseIdAsync(int courseId)
        {
            return await _context.CourseModule.Include(x => x.Module).Where(x => x.CourseId == courseId).Select(x => x.Module).ToListAsync();
        }
        public async Task RemoveModulesFromCourseAsync(List<int> notSelectedModulesId)
        {
            var range = await _context.Mark.Include(x => x.CourseModule).Where(x => notSelectedModulesId.Contains(x.CourseModule.ModuleId)).ToListAsync();

            _context.Comments.RemoveRange(await _context.Comments.Where(x => range.Select(y => y.MarkId).Contains(x.MarkId)).ToListAsync());
            _context.Mark.RemoveRange(range);
            _context.CourseModule.RemoveRange(await _context.CourseModule.Where(x => notSelectedModulesId.Contains(x.ModuleId)).ToListAsync());
        }
        public async Task IncludeNewModulesAsync(List<int> newModulesId, int courseId)
        {
            var existedModules = await _context.CourseModule.Where(cm => cm.CourseId == courseId).Select(x => x.ModuleId).ToListAsync();
            var newModules = newModulesId.Where(x => !existedModules.Contains(x)).Select(x => x).ToList();

            var courseStudents = await GetStudentsByCourseIdAsync(courseId);

            newModules.ForEach(moduleId =>
            {
                var cm = new CourseModule
                {
                    CourseId = courseId,
                    ModuleId = moduleId
                };

                _context.CourseModule.Add(cm);

                if (courseStudents != null && courseStudents.Count() > 0)
                {
                    foreach (var student in courseStudents)
                    {
                        _context.Mark.Add(
                           new Mark
                           {
                               StudentId = student.StudentId,
                               CourseModuleId = cm.CourseModuleId,
                           }
                       );
                    }
                }
            });
        }
        public async Task RemoveStudentsFromCourseAsync(List<int> notSelectedStudents, int courseId)
        {
            var courseModules = await _context.CourseModule.Where(x => x.CourseId == courseId).Select(x => x.CourseModuleId).ToListAsync();

            var range = await _context.Mark.Include(x => x.CourseModule).Where(x => notSelectedStudents.Contains(x.StudentId) && courseModules.Contains(x.CourseModuleId)).ToListAsync();
            _context.Comments.RemoveRange(await _context.Comments.Where(x => range.Select(y => y.MarkId).Contains(x.MarkId)).ToListAsync());
            _context.Mark.RemoveRange(range);
            _context.CourseStudent.RemoveRange(await _context.CourseStudent.Where(x => x.CourseId == courseId && notSelectedStudents.Contains(x.StudentId.Value)).ToListAsync());
        }
        public async Task IncludeNewStudentsAsync(List<int> newStudentsId, int courseId)
        {
            var courseModules = await _context.CourseModule.Where(x => x.CourseId == courseId).Select(x => x.CourseModuleId).ToListAsync();

            var existedStudents = await _context.CourseStudent.Where(cm => cm.CourseId == courseId).Select(x => x.StudentId).ToListAsync();
            var newStudents = newStudentsId.Where(x => !existedStudents.Contains(x)).Select(x => x).ToList();

            newStudents.ForEach(studentId =>
            {
                _context.CourseStudent.Add(
                    new CourseStudent
                    {
                        CourseId = courseId,
                        StudentId = studentId
                    }
                );

                courseModules.ForEach(courseModuleId =>
                {
                    _context.Mark.Add(new Mark
                    {
                        CourseModuleId = courseModuleId,
                        StudentId = studentId,
                    });
                });

            });
        }
        public async Task<IEnumerable<Student>> GetStudentsByCourseIdAsync(int courseId)
        {
            return await _context.CourseStudent
                .Include(x => x.Student)
                .Where(x => x.CourseId.Value == courseId)
                .Select(x => x.Student)
                .Include(x => x.CourseStudent)
                .Include(x => x.User)
                .ToListAsync();
        }
        public async Task<bool> RemoveModulesFromCourseAsync(int courseId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var courseModulesFromDB = await _context
                        .CourseModule
                        .Where(c => c.CourseId == courseId)
                        .ToListAsync();

                    foreach (CourseModule courseModule in courseModulesFromDB)
                    {
                        _context.Mark.RemoveRange(_context.Mark.Where(x => x.CourseModuleId == courseModule.CourseModuleId));
                    }
                    _context.CourseModule.RemoveRange(courseModulesFromDB);
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return false;
                }
            }
            return true;
        }
        public async Task<IEnumerable<CourseModule>> GetCourseModulesByCourseIdAsync(int courseId)
        {
            return await _context.CourseModule.Include(x => x.Module).Where(x => x.CourseId == courseId).ToListAsync();
        }
        public async Task<IEnumerable<CourseModule>> GetCourseModulesAsync(List<int> modulesId, int courseId)
        {
            return await _context
                    .CourseModule
                    .Where(y => y.CourseId == courseId && modulesId.Contains(y.ModuleId))
                    .ToListAsync();
        }
        public async Task<bool> UpdateCourseModulesAsync(List<CourseModule> courseModule)
        {
            _context.CourseModule.UpdateRange(courseModule);
            return await _context.SaveChangesAsync() >= 0;
        }



        public async Task<AspNetUsers> GetCourseTeacherAsync(int courseId)
        {
            Course course = await _context.Course.SingleOrDefaultAsync(c => c.CourseId == courseId);

            if (course.TeacherId != null)
            {
                return await _context.AspNetUsers.SingleOrDefaultAsync(u => u.Id == course.TeacherId);
            }
            return null;
        }
        public async Task<IEnumerable<CourseStudent>> GetCourseStudentsAsync(int courseId)
        {
            return await _context.CourseStudent.Include(cs => cs.Student).ThenInclude(s => s.User).Where(s => s.CourseId == courseId).ToListAsync();
        }
        public async Task<IEnumerable<Course>> GetTeacherCoursesAsync(string userId)
        {
            return await _context.Course.Include(c => c.CourseModule).ThenInclude(cm => cm.Module).Include(c => c.Teacher).Where(c => c.TeacherId == userId).ToListAsync();
        }

        public async Task<IEnumerable<Course>> GetCoursesRangeAsync(IEnumerable<int> ids)
        {
            return await _context.Course
                .Where(c => ids.Contains(c.CourseId))
                .Include(c => c.CourseModule)
                .ThenInclude(c => c.Module)
                .ToListAsync();
        }

        public async Task<CourseModule> GetCourseModuleByIdAsync(int id)
        {
            return await _context.CourseModule
                .Where(cm => cm.CourseModuleId == id)
                .Include(cm => cm.Course)
                .Include(cm => cm.Module)
                .SingleOrDefaultAsync();
        }
    }
}
