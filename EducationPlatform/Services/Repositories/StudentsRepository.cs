﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class StudentsRepository : IStudentsRepository
    {
        private EducationPlatformContext _context;

        public StudentsRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task<bool> HasStudentAccessAsync(string email)
        {
            var student = await _context.Student.Include(x => x.User).FirstOrDefaultAsync(x => x.User.Email == email);

            if (student == null)
            {
                return student.HasAccess.Value;
            }

            return false;
        }

        public async Task<bool> UpdateAsync(Student student)
        {
            _context.Student.Update(student);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteAsync(Student student)
        {
            _context.Student.Remove(student);
            _context.AspNetUsers.Remove(student.User);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Student> GetByIdAsync(int id)
        {
            return await _context.Student.Include(x => x.User).FirstOrDefaultAsync(x => x.StudentId == id);
        }
        public async Task<Student> GetByEmailAsync(string email)
        {
            return await _context.Student.Include(x => x.User).FirstOrDefaultAsync(x => x.User.Email == email);
        }

        public async Task<Student> GetByUserIdAsync(string id)
        {
            return await _context.Student.Include(x => x.User).FirstAsync(x => x.UserId == id);
        }

        public async Task<List<Student>> GetActiveNonBannedStudentsAsync()
        {
            return await _context.Student
                .Include(x => x.User)
                .Where(x => x.HasAccess.HasValue && x.HasAccess.Value && !(x.User.IsBanned.HasValue && x.User.IsBanned.Value))
                .ToListAsync();
        }

        public async Task<List<Student>> GetActiveStudentsAsync()
        {
            return await _context.Student
                .Include(x => x.User)
                .Include(x => x.CourseStudent)
                .Where(x => x.HasAccess.Value)
                .ToListAsync();
        }

        public async Task<List<Student>> GetEntrantsAsync()
        {
            return await _context.Student
                .Include(x => x.User)
                .Where(x => !x.HasAccess.Value)
                .ToListAsync();
        }

        public async Task<List<Student>> GetStudentsByCourseIdAsync(int id)
        {
            List<Student> students = new List<Student>();

            foreach(CourseStudent courseStudent in await _context.CourseStudent.Include(x => x.Student).Include(x => x.Student.User).Where(x => x.CourseId == id).ToListAsync())
            {
                students.Add(courseStudent.Student);
            }

            return students;
        }

        public async Task<List<CourseStudent>> GetCourseStudentByIdAsync(int id)
        {
            return (await _context.Student
                .Include(x => x.CourseStudent)
                .ThenInclude(x => x.Course)
                .ThenInclude(x => x.CourseModule)
                .ThenInclude(x => x.Module)
                .Include(s => s.CourseStudent)
                .ThenInclude(cs => cs.Course)
                .ThenInclude(c => c.Teacher)
                .FirstAsync(x => x.StudentId == id)).CourseStudent.ToList();
        }

        public async Task<List<Course>> GetStudentCoursesAsync(int id)
        {
            List<Course> courses = new List<Course>();

            foreach (CourseStudent courseStudent in await GetCourseStudentByIdAsync(id))
            {
                courses.Add(courseStudent.Course);
            }
            return courses;
        }
    }
}
