using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Modules;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class ModulesRepository : IModulesRepository
    {
        private EducationPlatformContext _context;
        private IHostingEnvironment _appEnvironment;

        public ModulesRepository(
            EducationPlatformContext context, 
            IHostingEnvironment appEnvironment
        )
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        //public async Task Insert(ModuleViewModel model)
        //{
        //    var mod = new Module
        //    {
        //        Name = model.Name,
        //        Description = model.Description,
        //        Duration = model.Duration,
        //        HasTest = model.HasTest,
        //        HasLab = model.HasLab,
        //        MinTestMark = model.MinTestMark,
        //        MaxTestMark = model.MaxTestMark,
        //        MinLabMark = model.MinLabMark,
        //        MaxLabMark = model.MaxLabMark,
        //        SubjectId = model.SubjectId
        //    };

        //    _context.Module.Add(mod);

        //    await _context.SaveChangesAsync();

        //    await SaveFilesToModules(model.Files, mod.ModuleId);
        //}

        public async Task Insert(Module module)
        {
            _context.Module.Add(module);

            await _context.SaveChangesAsync();
        }

        public Task<Module> GetById(int id)
        {
            return _context.Module
                .Include(x => x.Subject)
                .Include(x => x.ModuleFile)
                .Where(x => x.ModuleId == id)
                .FirstOrDefaultAsync();
        }


        //public async Task Update(ModuleViewModel model)
        //{
        //    var module = _context.Module.Where(m => m.ModuleId == model.ModuleId).FirstOrDefault();

        //    if (module != null)
        //    {
        //        module.Name = model.Name;
        //        module.Description = model.Description;
        //        module.Duration = model.Duration;
        //        module.HasTest = model.HasTest;
        //        module.HasLab = model.HasLab;
        //        module.MinTestMark = model.MinTestMark;
        //        module.MaxTestMark = model.MaxTestMark;
        //        module.MinLabMark = model.MinLabMark;
        //        module.MaxLabMark = model.MaxLabMark;

        //        _context.Module.Update(module);

        //        await SaveFilesToModules(model.Files, model.ModuleId);

        //        await _context.SaveChangesAsync();
        //    }

        //}

        public async Task Update(Module module)
        {
            _context.Module.Update(module);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Module>> GetModulesBySubjectId(int id)
        {
            IEnumerable<Module> modules = await _context.Module
                .Include(x => x.Subject)
                .Include(x => x.ModuleFile)
                .Where(x => x.SubjectId == id)
                .ToListAsync();

            return modules;
        }

        public async Task<List<ModuleFile>> GetModuleFiles(int moduleId)
        {
            return await _context.ModuleFile.Where(x => x.ModuleId == moduleId).ToListAsync();
        }

        public async Task Delete(Module module)
        {
            _context.ModuleFile.RemoveRange(module.ModuleFile);
            _context.Module.Remove(module);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(ICollection<Module> modules)
        {
            foreach(Module module in modules)
            {
                _context.ModuleFile.RemoveRange(module.ModuleFile);
                _context.Module.Remove(module);
            }

            await _context.SaveChangesAsync();
        }

        public void RemoveModuleFiles(IEnumerable<ModuleFile> files)
        {
            // var filesFromDB = await _context.ModuleFile.Where(x => x.ModuleId == moduleId).ToListAsync();

            //filesFromDB.ForEach(ffs => {
            //    if (File.Exists(_appEnvironment.WebRootPath + ffs.FileUrl))
            //    {
            //        File.Delete(_appEnvironment.WebRootPath + ffs.FileUrl);
            //    }
            //});

        }

        //public async Task<bool> SaveFilesToModules(List<IFormFile> files, int moduleId)
        //{
        //    try
        //    {
        //        if (moduleId > 0)
        //        {
        //            await RemoveFiles(moduleId);
        //        }

        //        if (files != null && files.Count > 0)
        //        {
        //            files.ForEach(async _file => {
        //                string path = "/ServerFiles/" + _file.FileName;
        //                // сохраняем файл в папку Files в каталоге wwwroot
        //                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
        //                {
        //                    await _file.CopyToAsync(fileStream);
        //                }


        //                ModuleFile moduleFile = new ModuleFile { FileUrl = path, ModuleId = moduleId };
        //                _context.ModuleFile.Add(moduleFile);
        //            });
        //        }

        //        await _context.SaveChangesAsync();

        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public async Task<List<CourseModule>> GetAllNextCourseModules()
        {
            var nextCourseModules = await _context.CourseModule
                .Include(cm => cm.Module)
                .Include(cm => cm.Course)
                .ThenInclude(c => c.CourseStudent)
                .Where(cm => cm.Date >= DateTime.Now || cm.StartTime >= DateTime.Now.TimeOfDay.Add(new TimeSpan(-2, 0, 0)))
                .OrderBy(cm => cm.Date)
                .ToListAsync();
            // nextCourseModules = nextCourseModules.OrderBy(cm => cm.Date).ToList();
            return nextCourseModules;
        }

        public async Task<List<CourseModule>> GetAllNextCourseModulesAvaliableForStudent(string channelId)
        {
            var userId = _context.BotAuthorizedUsers.Where(u => u.ChannelUserId == channelId).SingleOrDefault().UserId;
            var student = await _context.Student.Include(x => x.User).FirstAsync(x => x.UserId == userId);
            var nextCourseModules = await GetAllNextCourseModules();
            return nextCourseModules.Where(cm => cm.Course.CourseStudent.Any(cs => cs.StudentId == student.StudentId)).ToList();
        }

        public async Task<Module> GetModuleByIdAsync(int moduleId)
        {
            return await _context.Module.Where(x => x.ModuleId == moduleId).FirstOrDefaultAsync();
        }
    }
}
