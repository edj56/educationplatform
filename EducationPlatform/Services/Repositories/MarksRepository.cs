﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EducationPlatform.Services.Repositories
{
    public class MarksRepository : IMarksRepository
    {
        private readonly EducationPlatformContext _context;
        public MarksRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public void UpdateMark(Mark mark)
        {
            _context.Mark.Update(mark);
        }
        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() >= 0;
        }
        public async Task AddMarkAsync(Mark mark)
        {
            _context.Mark.Add(mark);
            await _context.SaveChangesAsync();
        }
        public async Task<Mark> GetMarkByIdAsync(int markId)
        {
            return await _context.Mark
                .Include(x => x.Student)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.Module)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.Course)
                .ThenInclude(x => x.Teacher)
                .Where(x => x.MarkId == markId)
                .FirstOrDefaultAsync();
        }

   

        public async Task PlaceTestMarkAsync(Mark testMark, int mark)
        {
            _context.Mark.FirstOrDefault(m => m.MarkId == testMark.MarkId).TestMark = mark;
            await _context.SaveChangesAsync();
        }
        public async Task PlaceLabMarkAsync(Mark labMark, int mark)
        {
            _context.Mark.FirstOrDefault(m => m.MarkId == labMark.MarkId).LabMark = mark;
            await _context.SaveChangesAsync();
        }


        public async Task<IEnumerable<Mark>> GetMarksByCourseModuleIdAsync(params int[] courseModuleId)
        {
            return await _context
                .Mark
                .Include(x => x.Student)
                .ThenInclude(x => x.User)
                .Include(x => x.CourseModule)
                .ThenInclude(x => x.Module)
                .Where(x => courseModuleId.Contains(x.CourseModuleId))
                .ToListAsync();
        }
        public async Task<IEnumerable<CourseStudent>> GetCourseStudentsByCourseIdAsync(int courseId)
        {
            return await _context
                 .CourseStudent
                 .Include(x => x.Student)
                 .ThenInclude(x => x.User)
                 .Include(x => x.Student)
                 .ThenInclude(x => x.Mark)
                 .Include(x => x.Course)
                 .ThenInclude(x => x.CourseModule)
                 .Where(x => x.CourseId == courseId)
                 .ToListAsync();
        }
        public async Task<IEnumerable<Mark>> GetMarksByStudentIdAndModuleIdAsync(int studentId, int moduleId)
        {
            return await _context.Mark.Where(m => m.StudentId == studentId).Where(m => m.CourseModuleId == moduleId).ToListAsync();
        }


        public async Task InsertCommentAsync(Comments comment)
        {
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Comments>> GetCommentByMarkIdAsync(int markId)
        {
            return await _context.Comments
                .Include(x => x.Mark)
                .Where(x => x.MarkId == markId)
                .ToListAsync();
        }
    }
}