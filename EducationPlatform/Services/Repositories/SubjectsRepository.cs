﻿using EducationPlatform.Models.ViewModels;
using EducationPlatform.Services.Interfaces;
using EducationPlatform.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using EducationPlatform.Models.ViewModels.Subjects;

namespace EducationPlatform.Services.Repositories
{
    public class SubjectsRepository : ISubjectsRepository
    {
        private EducationPlatformContext _context;

        public SubjectsRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Subject subject)
        {
            _context.Subject.Add(subject);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Subject subject)
        {
            _context.Subject.Update(subject);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Subject subject)
        {
            _context.Subject.Remove(subject);
            await _context.SaveChangesAsync();
        }

        public async Task<Subject> GetByIdAsync(int id)
        {
            return await _context.Subject.SingleOrDefaultAsync(subject => subject.SubjectId == id);
        }

        public async Task<Subject> GetWithModulesByIdAsync(int id)
        {
            return await _context.Subject
                .Where(subject => subject.SubjectId == id)
                .Include(subject => subject.Module)
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Subject>> GetSubjectsWithModulesAsync()
        {
            return await _context.Subject.Include(x => x.Module).ToListAsync();
        }
    }
}
