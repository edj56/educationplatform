﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class VideosRepository : IVideosRepository
    {
        private EducationPlatformContext _context;

        public VideosRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task InsertVideoAsync(Video video)
        {
            _context.Video.Add(video);
            await _context.SaveChangesAsync();
        }

        public async Task InsertCourseModuleVideoAsync(CourseModuleVideo courseModuleVideo)
        {
            _context.CourseModuleVideo.Add(courseModuleVideo);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateVideoAsync(int id, string link)
        {
            var video = _context.Video.SingleOrDefault(v => v.VideoId == id);
            video.Link = link;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteVideoAsync(int videoId, int courseModuleId)
        {
            var video = _context.Video.Include(c => c.CourseModuleVideo).SingleOrDefault(v => v.VideoId == videoId);

            if (video.CourseModuleVideo.Count > 1)
            {
                var courseModuleVideo = _context.CourseModuleVideo.SingleOrDefault(c => c.CourseModuleId == courseModuleId && c.VideoId == videoId);
                _context.CourseModuleVideo.Remove(courseModuleVideo);
            }
            else
            {
                _context.Video.Remove(video);
            }

            await _context.SaveChangesAsync();
        }

        public async Task DeleteVideoAsync(Video video)
        {
            _context.Remove(video);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteCourseModuleVideoAsync(CourseModuleVideo courseModuleVideo)
        {
            _context.CourseModuleVideo.Remove(courseModuleVideo);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<SelectListItem>> GetCoursesForDropdownAsync()
        {
            List<SelectListItem> courses = await _context.Course.Select(n =>
                        new SelectListItem
                        {
                            Value = n.CourseId.ToString(),
                            Text = n.Name
                        }).ToListAsync();

            return new SelectList(courses, "Value", "Text");
        }

        public async Task<IEnumerable<SelectListItem>> GetCourseModulesForDropdownAsync(int id)
        {
            IEnumerable<SelectListItem> courseModules = await _context.CourseModule
                        .Include(c => c.Module)
                        .Where(c => c.CourseId == id)
                        .Select(c =>
                           new SelectListItem
                           {
                               Value = c.CourseModuleId.ToString(),
                               Text = c.Module.Name
                           }).ToListAsync();

            return new SelectList(courseModules, "Value", "Text");
        }

        public async Task<Video> GetVideoByLinkAsync(string link)
        {
            return await _context.Video.Include(c => c.CourseModuleVideo).SingleOrDefaultAsync(v => v.Link == link);
        }

        public async Task<CourseModuleVideo> GetCourseModuleVideoAsync(int videoId, int courseModuleId)
        {
            return await _context.CourseModuleVideo
                .Include(v => v.Video)
                .Include(c => c.CourseModule)
                .ThenInclude(c => c.Course)
                .Include(c => c.CourseModule)
                .ThenInclude(m => m.Module)
                .SingleOrDefaultAsync(c => c.CourseModuleId == courseModuleId && c.VideoId == videoId);
        }

        public async Task<Video> GetVideoByIdAsync(int id)
        {
            return await _context.Video.Include(c => c.CourseModuleVideo).SingleOrDefaultAsync(v => v.VideoId == id);
        }

    }

}
