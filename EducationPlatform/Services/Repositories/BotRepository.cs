﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class BotRepository : IBotRepository
    {
        private readonly EducationPlatformContext _context;

        public BotRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task<BotAuthorizationKeys> GetUserByKey(string key)
        {
            return await _context.BotAuthorizationKeys
                .Where(keys => keys.AuthorizationKey == key)
                .Include(k => k.User)
                .SingleOrDefaultAsync();
        }

        public async Task<BotAuthorizedUsers> GetBotAuthorizedUser(string userId)
        {
            return await _context.BotAuthorizedUsers
                .Where(user => user.UserId == userId)
                .Include(k => k.User)
                .SingleOrDefaultAsync();
        }

        public async Task CreateBotAuthorizedUser(string channelId, string userId, string channelUserId, string conversationId)
        {
            _context.BotAuthorizedUsers.Add(new BotAuthorizedUsers
            {
                ChannelId = channelId,
                UserId = userId,
                ChannelUserId = channelUserId,
                ConversationId = conversationId
            });

            await _context.SaveChangesAsync();
        }

        public async Task DeleteBotAuthorizedUser(BotAuthorizedUsers botAuthorizedUser)
        {
            _context.BotAuthorizedUsers.Remove(botAuthorizedUser);

            await _context.SaveChangesAsync();
        }

        public List<MessengersBots> GetMessangersBots()
        {
            return _context.MessengersBots.ToList();
        }

        public async Task<bool> CheckUserBotChannelId(string userId, string channelId)
        {
            return await _context.BotAuthorizedUsers
                .Where(u => u.UserId == userId && u.ChannelId == channelId)
                .Include(k => k.User)
                .SingleOrDefaultAsync() != null;
        }

        public async Task CreateAuthorizationKey(BotAuthorizationKeys key)
        {
            _context.BotAuthorizationKeys.Add(key);
            await _context.SaveChangesAsync();
        }

        public async Task<BotAuthorizationKeys> GetAuthorizationKey(string userId)
        {
            return await _context.BotAuthorizationKeys
                .Where(key => key.UserId == userId)
                .Include(k => k.User)
                .SingleOrDefaultAsync();
        }

        public async Task<BotAuthorizationKeys> GetAuthorizationKeyByKey(string key)
        {
            return await _context.BotAuthorizationKeys
                .Where(k => k.AuthorizationKey == key)
                .Include(k => k.User)
                .SingleOrDefaultAsync();
        }

        public async Task<string> GetAuthorizedUserId(string channelUserId)
        {
            return (await _context.BotAuthorizedUsers.Where(u => u.ChannelUserId == channelUserId).SingleOrDefaultAsync())?.UserId;
        }

        public async Task DeleteAuthorizationKey(BotAuthorizationKeys key)
        {
            _context.BotAuthorizationKeys.Remove(key);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> CheckBotUserAuthorization(string channelUserId)
        {
            return await _context.BotAuthorizedUsers.Where(user => user.ChannelUserId == channelUserId).SingleOrDefaultAsync() != null;
        }
    }
}
