﻿using EducationPlatform.Models.Entities;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels;
using EducationPlatform.Models.ViewModels.Account;
using EducationPlatform.Models.ViewModels.Students;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly EducationPlatformContext _context;

        public UsersRepository(EducationPlatformContext context)
        {
            _context = context;
        }

        public async Task<bool> AddUserRoleAsync(AspNetUserRoles userRole)
        {
            _context.AspNetUserRoles.Add(userRole);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<AspNetRoles> FindRoleByNameAsync(string name)
        {
            return await _context.AspNetRoles.FirstAsync(x => x.Name == name);
        }

        public async Task<List<AspNetUserRoles>> GetUserRolesByIdAsync(string id)
        {
            return await _context.AspNetUserRoles.Where(role => role.UserId == id).ToListAsync();
        }

        public async Task RemoveUserRolesAsync(List<AspNetUserRoles> userRoles)
        {
            _context.AspNetUserRoles.RemoveRange(userRoles);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> CheckIfUserIsStudentByIdAsync(string id)
        {
            return await _context.Student.Select(student => student.UserId).ContainsAsync(id);
        }

        public async Task<bool> CreateStudentAsync(Student student)
        {
            _context.Student.Add(student);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task AddUserLoginsAsync(AspNetUserLogins userLogins)
        {
            _context.AspNetUserLogins.Add(userLogins);
            await _context.SaveChangesAsync();
        }

        public async Task<AspNetUserLogins> GetAspNetUserLoginsByUserId(string userId)
        {
            return await _context.AspNetUserLogins.Include(userLogin => userLogin.User).FirstOrDefaultAsync(userLogin => userLogin.UserId == userId);
        }

        public async Task<bool> UpdateAsync(AspNetUsers user)
        {
            _context.AspNetUsers.Update(user);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task AddTwoFactorUserAsync(TwoFactorUser user)
        {
            _context.TwoFactorUser.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateTwoFactorUserAsync(TwoFactorUser user)
        {
            _context.TwoFactorUser.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteTwoFactorUserAsync(TwoFactorUser user)
        {
            _context.TwoFactorUser.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<TwoFactorUser> GetTwoFactorUserByIdAsync(string id)
        {
            return await _context.TwoFactorUser.Include(twoFactorUser => twoFactorUser.User).FirstOrDefaultAsync(twoFactorUser => twoFactorUser.UserId == id);
        }

        public async Task<IEnumerable<AspNetUsers>> GetTeachers()
        {
            return await _context.AspNetUsers
                .Include(user => user.Course)
                .Include(user => user.Student)
                .Where(user => user.Student == null && !user.AspNetUserRoles.Any(role => role.Role.Name == "Admin"))
                .ToListAsync();
        }

        public async Task<IEnumerable<AspNetUsers>> GetNotBannedUsersInRole(string roleName)
        {
            return await _context.AspNetUsers.Where(x => x.AspNetUserRoles.Any(y => y.Role.Name == roleName)).Where(x => x.IsBanned == false).ToListAsync();
        }

        public async Task<AspNetUsers> GetUserByIdAsync(string id)
        {
            return await _context.AspNetUsers.SingleOrDefaultAsync(user => user.Id == id);
        }

        public async Task<AspNetUsers> GetUserByEmailAsync(string email)
        {
            return await _context.AspNetUsers.SingleOrDefaultAsync(user => user.Email == email);
        }

        public async Task<string> GetUserRoleAsync(string userId)
        {
            AspNetUsers user = await _context.AspNetUsers.Include(u => u.AspNetUserRoles).Where(u => u.Id == userId).SingleOrDefaultAsync();

            string roleId = "";

            if (user == null)
            {
                return null;
            }

            foreach (AspNetUserRoles r in user.AspNetUserRoles)
            {
                roleId = r.RoleId;
            }

            AspNetRoles role = await _context.AspNetRoles.Where(r => r.Id == roleId).SingleOrDefaultAsync();

            if (role != null)
            {
                return role.Name;
            }

            return null;
        }
        
        public async Task<AspNetUsers> GetTeacherByMarkId(int markId)
        {
            var mark = await _context.Mark
               .Where(x => x.MarkId == markId)
               .Include(x => x.CourseModule)
               .ThenInclude(x => x.Course)
               .ThenInclude(x => x.Teacher)
               .SingleOrDefaultAsync();

            return mark.CourseModule.Course.Teacher;
        }
    }
}
