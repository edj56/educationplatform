﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Videos;
using EducationPlatform.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class VideosService : IVideosService
    {
        private readonly IVideosRepository _videosRepository;
        private readonly ICoursesRepository _courseRepository;

        public VideosService(
             ICoursesRepository coursesRepository,
             IVideosRepository videosRepository
        )
        {
            _courseRepository = coursesRepository;
            _videosRepository = videosRepository;
        }

        public async Task<VideoViewModel> GetVideoViewModelAsync()
        {
            var course = await _courseRepository.GetFirstCourseAsync();
            var videoViewModel = new VideoViewModel
            {
                Courses = await _videosRepository.GetCoursesForDropdownAsync(),
                CourseModules = await _videosRepository.GetCourseModulesForDropdownAsync(course.CourseId)
            };

            return videoViewModel;
        }

        public async Task AddVideoAsync(VideoViewModel model)
        {
            var video = await _videosRepository.GetVideoByLinkAsync(model.Link.Substring(model.Link.Length - 11));

            if (video == null)
            {
                video = new Video
                {
                    Link = model.Link.Substring(model.Link.Length - 11)
                };

                await _videosRepository.InsertVideoAsync(video);
            }

            var courseModuleVideo = new CourseModuleVideo
            {
                CourseModuleId = model.CourseModuleId,
                VideoId = video.VideoId
            };

            await _videosRepository.InsertCourseModuleVideoAsync(courseModuleVideo);
        }

        public async Task<VideoViewModel> GetVideoViewModelAsync(int videoId, int courseModuleId)
        {
            var courseModuleVideo = await _videosRepository.GetCourseModuleVideoAsync(videoId, courseModuleId);
            int courseId = courseModuleVideo.CourseModule.Course.CourseId;

            var videoViewModel = new VideoViewModel
            {
                VideoId = videoId,
                Link = $"https://youtu.be/{courseModuleVideo.Video.Link}",
                Courses = await _videosRepository.GetCoursesForDropdownAsync(),
                CourseId = courseId,
                CourseModules = await _videosRepository.GetCourseModulesForDropdownAsync(courseId),
                CourseModuleId = courseModuleId,
                OldCourseModuleId = courseModuleId
            };

            return videoViewModel;
        }

        public async Task EditVideoAsync(VideoViewModel model)
        {
            var video = await _videosRepository.GetVideoByLinkAsync(model.Link.Substring(model.Link.Length - 11));
            var oldVideo = await _videosRepository.GetVideoByIdAsync(model.VideoId);
            var courseModuleVideo = await _videosRepository.GetCourseModuleVideoAsync(model.VideoId, model.OldCourseModuleId);

            await _videosRepository.DeleteCourseModuleVideoAsync(courseModuleVideo);

            if (oldVideo.CourseModuleVideo.Count == 0 && video == null)
            {
                video = oldVideo;
                await _videosRepository.UpdateVideoAsync(model.VideoId, model.Link.Substring(model.Link.Length - 11));
            }
            else if (video == null)
            {
                video = new Video
                {
                    Link = model.Link.Substring(model.Link.Length - 11)
                };

                await _videosRepository.InsertVideoAsync(video);
            }

            await _videosRepository.InsertCourseModuleVideoAsync(new CourseModuleVideo
            {
                CourseModuleId = model.CourseModuleId,
                VideoId = video.VideoId
            });

            var modelVideo = await _videosRepository.GetVideoByIdAsync(model.VideoId);

            if (modelVideo.CourseModuleVideo.Count == 0)
            {
                await _videosRepository.DeleteVideoAsync(oldVideo);

            }
        }

        public async Task DeleteVideoAsync(int videoId, int courseModuleId)
        {
            await _videosRepository.DeleteVideoAsync(videoId, courseModuleId);
        }

        public async Task<JsonResult> GetModulesForDdropdownAsync(string id)
        {
            IEnumerable<SelectListItem> modules = await _videosRepository.GetCourseModulesForDropdownAsync(Convert.ToInt32(id));
            return new JsonResult(modules);
        }

        public Task<IEnumerable<Course>> GetCourses()
        {
            return _courseRepository.GetCoursesAsync();
        }
    }
}
