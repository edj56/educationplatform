﻿using System;
using System.Collections.Generic;
using System.Linq;
using EducationPlatform.Services.Interfaces;
using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Schedule;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly ICoursesRepository _coursesRepository;
        private readonly IModulesService _modulesService;

        public ScheduleService(
            ICoursesRepository coursesRepository,
            IModulesService modulesService
        )
        {
            _coursesRepository = coursesRepository;
            _modulesService = modulesService;

        }
        public async Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByStudent(string Id)
        {
            var courses = (await _coursesRepository.GetCoursesAsync()).Where(z => z.CourseStudent.Select(x => x.Student.UserId).Contains(Id));
            return GetJsonData(courses);
        }

        public async Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByTeacher(string email)
        {
            var coureses = (await _coursesRepository.GetCoursesAsync()).Where(z => z.Teacher.UserName == email);
            return GetJsonData(coureses);
        }

        public async Task<IEnumerable<ScheduleItemViewModel>> GetScheduleByAllCourses()
        {
            var courses = await _coursesRepository.GetCoursesAsync();
            return GetJsonData(courses);
        }

        public async Task<IEnumerable<ScheduleItemViewModel>> GetCoursesSchedulebyFilter(List<int> coursesId)
        {
            List<IEnumerable<ScheduleItemViewModel>> courses = new List<IEnumerable<ScheduleItemViewModel>>();
            foreach (var courseId in coursesId)
            {
                courses.Add(GetJsonData(new List<Course>() { await _coursesRepository.GetByIdAsync(courseId) }));
            }

            return CreateSingleListOfSchedule(courses);
        }
        public async Task<IEnumerable<ScheduleItemViewModel>> GetCoursesScheduleByTeacherFilter(List<string> listEmails)
        {
            List<IEnumerable<ScheduleItemViewModel>> courses = new List<IEnumerable<ScheduleItemViewModel>>();
            foreach (var email in listEmails)
            {
                courses.Add(await GetScheduleByTeacher(email));
            }
            return CreateSingleListOfSchedule(courses);
        }


        private IEnumerable<ScheduleItemViewModel> CreateSingleListOfSchedule(List<IEnumerable<ScheduleItemViewModel>> courses)
        {
            List<ScheduleItemViewModel> schedule = new List<ScheduleItemViewModel>();
            foreach (var courseSchedule in courses)
            {
                foreach (var data in courseSchedule)
                {
                    schedule.Add(data);
                }
            }
            return schedule;
        }

        private IEnumerable<ScheduleItemViewModel> GetJsonData(IEnumerable<Course> courses)
        {
            IEnumerable<ScheduleItemViewModel> schedules;
            schedules = courses.SelectMany(x => x.CourseModule).Select(x => new
            {
                module = _modulesService.GetById(x.ModuleId).Result,
                course = x.Course.Name,
                start = (x.Date.HasValue ? x.Date.Value.ToString("yyyy-MM-dd") : DateTime.MinValue.ToString("yyyy-MM-dd")) + " " + x.StartTime,
                end = (x.Date.HasValue ? x.Date.Value.ToString("yyyy-MM-dd") : DateTime.MinValue.ToString("yyyy-MM-dd")) + " " + (x.StartTime + (x.Module.Duration != null ? x.Module.Duration : TimeSpan.FromHours(2)))
            }).Select(x => new ScheduleItemViewModel
            {
                Title = x.course + ": " + x.module.Name,
                Description = x.module.Description,
                Start = x.start,
                End = x.end
            });
            return schedules;
        }
    }
}
