﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Bot;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Modules;
using EducationPlatform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EducationPlatform.Services
{
    public class BotService : IBotService
    {
        private readonly ICoursesRepository _coursesRepository;
        private readonly IBotRepository _botRepository;
        private readonly IStudentsRepository _studentsRepository;

        private readonly HttpClient httpClient = new HttpClient();

        public BotService
        (
            ICoursesRepository coursesRepository,
            IBotRepository botRepository,
            IStudentsRepository studentsRepository
        )
        {
            _coursesRepository = coursesRepository;
            _botRepository = botRepository;
            _studentsRepository = studentsRepository;
        }

        public async Task<PushNotificationsViewModel> PrepareForPushNotifications(CourseEditSchedulesViewModel model)
        {
            List<MessengersBots> messangersBots = _botRepository.GetMessangersBots();
            List<CourseStudent> courseStudents = (await _coursesRepository.GetCourseStudentsAsync(model.CourseId)).ToList();
            Dictionary<string, string> jsons = new Dictionary<string, string>();

            foreach (MessengersBots bot in messangersBots)
            {
                List<string> idList = new List<string>();

                foreach (CourseStudent courseStudent in courseStudents)
                {
                    Student student = await _studentsRepository.GetByIdAsync((int)courseStudent.StudentId);

                    if (await _botRepository.CheckUserBotChannelId(student.UserId, bot.ChannelId))
                    {
                        idList.Add(student.UserId);
                    }
                }

                AspNetUsers teacher = await _coursesRepository.GetCourseTeacherAsync(model.CourseId);

                if (teacher != null)
                {
                    idList.Add(teacher.Id);
                }

                StringBuilder channelUserId = new StringBuilder();
                StringBuilder conversationId = new StringBuilder();

                foreach (string id in idList)
                {
                    BotAuthorizedUsers user = await _botRepository.GetBotAuthorizedUser(id);

                    if (user != null)
                    {
                        channelUserId.Append($"\"{user.ChannelUserId}\", ");
                        conversationId.Append($"\"{user.ConversationId}\", ");
                    }
                }

                channelUserId.Length -= 2;
                conversationId.Length -= 2;

                Course course = await _coursesRepository.GetByIdAsync(model.CourseId);
                StringBuilder modules = new StringBuilder();

                foreach (CourseModuleEditViewModel module in model.Modules)
                {
                    CourseModule oldModule = course.CourseModule.FirstOrDefault(x => x.ModuleId == module.Id);
                    string oldModuleString = oldModule.Date != null ? $"{string.Format("{0:dd.MM.yyyy}", oldModule.Date)} {string.Format("{0:hh\\:mm}", oldModule.StartTime)}" : "";
                    modules.Append($"\\n\\n* {oldModule.Module.Name}: *{oldModuleString}* - **{string.Format("{0:dd.MM.yyyy}", module.Date)} {string.Format("{0:hh\\:mm}", module.StartTime)}**");
                }

                string messageText = $"Зміни у розкладі на курс **{course.Name}**.{modules.ToString()}";
                jsons.Add(bot.BotId, ($"{{ \"channelUserId\": [{channelUserId.ToString()}], \"conversationId\": [{conversationId.ToString()}], \"channelId\": \"{bot.ChannelId}\", \"type\": \"message\", \"text\": \"{messageText}\", \"recipient\": \"{bot.BotId}\", \"url\": \"{bot.BotUrl}\", \"serviceUrl\": \"{bot.ServiceUrl}\" }}"));
            }

            return new PushNotificationsViewModel
            {
                Jsons = jsons,
                MessangersBots = messangersBots
            };
        }

        public async Task SendPushNotificationsAsync(PushNotificationsViewModel model)
        {
            foreach (MessengersBots bot in model.MessangersBots)
            {
                await httpClient.PostAsync(
                    $"{bot.BotUrl}/pushNotifications",
                    new StringContent(model.Jsons[bot.BotId], Encoding.UTF8, "application/json")
                );
            }
        }

        public async Task<BotAuthorizationKeysViewModel> GetUserByKey(string key)
        {
            BotAuthorizationKeys botKey = await _botRepository.GetUserByKey(key);

            if(botKey != null)
            {
                return new BotAuthorizationKeysViewModel(botKey);
            }

            return null;
        }

        public async Task<BotAuthorizedUsersViewModel> GetBotAuthorizedUser(string userId)
        {
            BotAuthorizedUsers botKey = await _botRepository.GetBotAuthorizedUser(userId);

            if(botKey != null)
            {
                return new BotAuthorizedUsersViewModel(botKey);
            }

            return null;
        }

        public async Task CreateBotAuthorizedUser(string channelId, string userId, string channelUserId, string conversationId)
        {
            await _botRepository.CreateBotAuthorizedUser(channelId, userId, channelUserId, conversationId);
        }

        public async Task DeleteBotAuthorizedUser(BotAuthorizedUsersViewModel botAuthorizedUser)
        {
            await _botRepository.DeleteBotAuthorizedUser(await _botRepository.GetBotAuthorizedUser(botAuthorizedUser.UserId));
        }

        public async Task<bool> CheckBotUserAuthorization(string channelUserId)
        {
            return await _botRepository.CheckBotUserAuthorization(channelUserId);
        }

        public async Task<string> GetAuthorizedUserId(string channelUserId)
        {
            return await _botRepository.GetAuthorizedUserId(channelUserId);
        }

        public async Task<BotAuthorizationKeysViewModel> GetAuthorizationKeyByKey(string key)
        {
            BotAuthorizationKeys botKey = await _botRepository.GetAuthorizationKeyByKey(key);

            if (botKey != null)
            {
                return new BotAuthorizationKeysViewModel(botKey);
            }

            return null;
        }

        public async Task DeleteAuthorizationKey(BotAuthorizationKeysViewModel botKey)
        {
            await _botRepository.DeleteAuthorizationKey(await _botRepository.GetAuthorizationKey(botKey.UserId));
        }

        public async Task<BotAuthorizationKeysViewModel> GetAuthorizationKey(string id)
        {
            BotAuthorizationKeys key = await _botRepository.GetAuthorizationKey(id);

            if(key != null)
            {
                return new BotAuthorizationKeysViewModel(key);
            }

            return null;
        }

        public async Task CreateAuthorizationKey(string id)
        {
            string key = GenerateRandomKey();

            while (await GetUserByKey(key) != null)
            {
                key = GenerateRandomKey();
            }

            await _botRepository.CreateAuthorizationKey(new BotAuthorizationKeys
            {
                AuthorizationKey = key,
                UserId = id
            });
        }

        private string GenerateRandomKey()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            int length = 8;

            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
