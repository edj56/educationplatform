﻿using EducationPlatform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MailKit.Security;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using System.Text;

namespace EducationPlatform.Services
{
    public class EmailSender : IEmailSender
    {
        private IHostingEnvironment _environment;
        private IHttpContextAccessor _httpContextAccessor;

        private string _title;
        private string _email;
        private string _password;
        private string _host;
        private int _port;

        public EmailSender(IConfiguration configuration, IHostingEnvironment environment, IHttpContextAccessor httpContextAccessor)
        {
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;

        var mail = configuration.GetSection("Mail");

            _title = mail.GetValue<string>("Title");
            _email = mail.GetValue<string>("Address");
            _password = mail.GetValue<string>("Password");
            _host = mail.GetValue<string>("Host");
            _port = mail.GetValue<int>("Port");
        }

        public async Task SendDefaultEmailAsync(string email, string subject, string message, string buttonLink, string buttonText)
        {
            await SendEmailAsync(email, subject, CreateDeafaulHtmlBody(message, buttonLink, buttonText));
        }

        public async Task SendMarkChatEmailAsync(string email, string subject, string message, string userFromFullName, string emailFrom, string courseName, string moduleName, int? testMark, int? labMark, string buttonLink)
        {
            await SendEmailAsync(email, subject, CreateMarkChatHtmlBody(userFromFullName, emailFrom, courseName, moduleName, testMark, labMark, message, buttonLink));
        }

        private async Task SendEmailAsync(string email, string subject, string htmlBody)
        {
            MimeMessage emailMessage = CreateMimeMessage(email, subject, htmlBody);

            using (var client = new SmtpClient())
            {
                try
                {
                    // during development
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    await client.ConnectAsync(_host, _port, SecureSocketOptions.StartTlsWhenAvailable);
                    await client.AuthenticateAsync(_email, _password);
                    await client.SendAsync(emailMessage);
                    await client.DisconnectAsync(true);
                }
                catch (Exception)
                { }
            }
        }

        private MimeMessage CreateMimeMessage(string email, string subject, string htmlBody)
        {
            MimeMessage emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_title, _email));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = htmlBody };

            return emailMessage;
        }

        private string CreateDeafaulHtmlBody(string message, string buttonLink, string buttonText)
        {
            string body = "";
            var webRoot = _environment.WebRootPath;
            var htmlFile = Path.Combine(webRoot, "lib", "template", "email-letter-template.html");

            using (StreamReader reader = new StreamReader(htmlFile))
            {
                body = reader.ReadToEnd();
            }

            string domainUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.ToString()}";

            body = body.Replace("{CONTENT}", message);
            body = body.Replace("{domain-url}", domainUrl);
            body = body.Replace("{button-link}", buttonLink);
            body = body.Replace("{button-text}", buttonText);

            return body;
        }

        private string CreateMarkChatHtmlBody(string userFromFullName, string emailFrom, string courseName, string moduleName, int? testMark, int? labMark, string message, string buttonLink)
        {
            string body = "";
            var webRoot = _environment.WebRootPath;
            var htmlFile = Path.Combine(webRoot, "lib", "template", "email-mark-chat-letter-template.html");

            using (StreamReader reader = new StreamReader(htmlFile))
            {
                body = reader.ReadToEnd();
            }

            string domainUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.ToString()}";

            if (message.Length > 500)
            {
                body = body.Replace("{COMMENT}", "<p>Довжина коментаря більша за 500 символів, перейдіть за посиланням вище, щоб переглянути його</p>");
            }
            else
            {
                body = body.Replace("{COMMENT}", message);
            }

            if (labMark == null || labMark == 0)
            {
                body = body.Replace("{lab-mark}", "Немає");
            }
            else
            {
                body = body.Replace("{lab-mark}", labMark.ToString());
            }

            if (testMark == null || testMark == 0)
            {
                body = body.Replace("{test-mark}", "Немає");
            }
            else
            {
                body = body.Replace("{test-mark}", testMark.ToString());
            }

            body = body.Replace("{user-from-full-name}", userFromFullName);
            body = body.Replace("{emailHash}", HashEmailForGravatar(emailFrom));
            body = body.Replace("{course-name}", courseName);
            body = body.Replace("{module-name}", moduleName);
            body = body.Replace("{button-link}", buttonLink);
            body = body.Replace("{domain-url}", domainUrl);

            return body;
        }

        private string HashEmailForGravatar(string email)
        {
            var md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.ASCII.GetBytes(email.ToLower()));
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append(data[i].ToString("x2"));
            }

            return stringBuilder.ToString();
        }
    }
}
