﻿using EducationPlatform.Models.EntityModels;
using EducationPlatform.Models.ViewModels.Courses;
using EducationPlatform.Models.ViewModels.Marks;
using EducationPlatform.Models.ViewModels.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationPlatform.Services.Interfaces
{
    public class MarksServices : IMarksServices
    {
        private readonly ICoursesRepository _coursesRepository;
        private readonly IModulesRepository _modulesRepository;
        private readonly IStudentsRepository _studentsRepository;
        private readonly IMarksRepository _marksRepository;

        public MarksServices(ICoursesRepository coursesRepository, IModulesRepository modulesRepository, IStudentsRepository studentsRepository, IMarksRepository marksRepository)
        {
            _coursesRepository = coursesRepository;
            _modulesRepository = modulesRepository;
            _studentsRepository = studentsRepository;
            _marksRepository = marksRepository;
        }



        public async Task AddMarkAsync(Mark mark)
        {
            await _marksRepository.AddMarkAsync(mark);
        }
        public async Task<Mark> GetMarkByIdAsync(int markId)
        {
            return await _marksRepository.GetMarkByIdAsync(markId);
        }


        public async Task PlaceTestMarkAsync(Mark testMark, int mark)
        {
            await _marksRepository.PlaceTestMarkAsync(testMark, mark);
        }
        public async Task PlaceLabMarkAsync(Mark labMark, int mark)
        {
            await _marksRepository.PlaceLabMarkAsync(labMark, mark);
        }


        public async Task<IEnumerable<Mark>> GetMarksByStudentIdAndModuleIdAsync(int studentId, int moduleId)
        {
            return await _marksRepository.GetMarksByStudentIdAndModuleIdAsync(studentId, moduleId);
        }

        public async Task<List<MarkViewModel>> GetMarksViewModelsByStudentIdAndModuleIdAsync(int studentId, int moduleId)
        {
            List<MarkViewModel> marks = new List<MarkViewModel>();
            foreach (var mark in await _marksRepository.GetMarksByStudentIdAndModuleIdAsync(studentId, moduleId))
            {
                marks.Add(new MarkViewModel(mark));
            }
            return marks;
        }


        public async Task InsertCommentAsync(Comments comment)
        {
            await _marksRepository.InsertCommentAsync(comment);
        }
        public async Task<IEnumerable<Comments>> GetCommentByMarkIdAsync(int markId)
        {
            return await _marksRepository.GetCommentByMarkIdAsync(markId);
        }


        public async Task<CourseModuleMarksViewModel> GetModuleStudentsMarksAsync(int courseId, int moduleId)
        {
            var course = await _coursesRepository.GetByIdAsync(courseId);
            var courseModule = course.CourseModule.Where(x => x.ModuleId == moduleId).First();


            var marks = (await _marksRepository.GetMarksByCourseModuleIdAsync(courseModule.CourseModuleId)).Select(x => new CourseModuleStudentsMark
            {
                CourseModuleId = x.CourseModuleId,
                TestMark = x.TestMark ?? 0,
                LabMark = x.LabMark ?? 0,
                StudentId = x.StudentId,
                StudentName = x.Student.User.LastName + " " + x.Student.User.FirstName + " " + x.Student.User.MiddleName
            }).ToList();


            return new CourseModuleMarksViewModel
            {
                Course = course,
                CurrentModule = await _modulesRepository.GetModuleByIdAsync(moduleId),
                Marks = marks,
                Subject = course.Subject
            };
        }
        public async Task<bool> UpdateModuleStudentsMarksAsync(CourseModuleMarksViewModel model)
        {
            var courseModuleIds = model.Marks.Select(x => x.CourseModuleId).ToArray();
            var marksToUpdate = (await _marksRepository.GetMarksByCourseModuleIdAsync(courseModuleIds))
                .ToList();

            model.Marks.ForEach(x =>
            {
                var toUpdate = marksToUpdate.First(y => y.StudentId == x.StudentId);

                toUpdate.LabMark = x.LabMark;
                toUpdate.TestMark = x.TestMark;

                _marksRepository.UpdateMark(toUpdate);
            });

            return await _marksRepository.SaveAsync();
        }
        public async Task<bool> UpdateStudentModulesMarksAsync(CourseStudentMarksViewModel model)
        {
            var courseModuleIds = model.Marks.Select(x => x.CourseModuleId).ToArray();
            var marksToUpdate = (await _marksRepository.GetMarksByCourseModuleIdAsync(courseModuleIds))
                .Where(x => x.StudentId == model.CurrentStudent.StudentId)
                .ToList();

            model.Marks.ForEach(x =>
            {
                var toUpdate = marksToUpdate.First(y => y.CourseModule.ModuleId == x.ModuleId);

                toUpdate.LabMark = x.LabMark;
                toUpdate.TestMark = x.TestMark;

                _marksRepository.UpdateMark(toUpdate);
            });

            return await _marksRepository.SaveAsync();
        }
        public async Task<CourseStudentMarksViewModel> GetStudentModulesMarksAsync(int courseId, int studentId)
        {
            var course = await _coursesRepository.GetByIdAsync(courseId);
            var courseModules = course.CourseModule.ToList();
            var courseModulesIds = courseModules.Select(x => x.CourseModuleId).ToArray();

            var marks = (await _marksRepository.GetMarksByCourseModuleIdAsync(courseModulesIds))
                .Where(x => x.StudentId == studentId)
                .Select(x => new CourseStudentModulesMark
                {
                    CourseModuleId = x.CourseModuleId,
                    TestMark = x.TestMark ?? 0,
                    LabMark = x.LabMark ?? 0,
                    ModuleId = x.CourseModule.ModuleId,
                    ModuleName = x.CourseModule.Module.Name,
                    Module = x.CourseModule.Module,
                    MarkId = x.MarkId
                }).ToList();

            return new CourseStudentMarksViewModel
            {
                Course = course,
                CurrentStudent = await _studentsRepository.GetByIdAsync(studentId),
                Marks = marks,
                Subject = course.Subject
            };
        }
        public async Task<CourseRatingViewModel> GetRatingAsync(int courseId)
        {
            var course = await _coursesRepository.GetByIdAsync(courseId);

            var courseStudents = await _marksRepository.GetCourseStudentsByCourseIdAsync(courseId);

            var marks = new List<CourseModuleStudentsMark>();

            var courseModules = await _coursesRepository.GetCourseModulesAsync();

            foreach (var student in courseStudents)
            {
                var courseMod = student.Student.Mark;
                var marksInCourseByStudent = student.Student.Mark
                    .Where(y => y.StudentId == student.StudentId)
                    .Where(y => courseModules
                    .Where(z => y.CourseModuleId == z.CourseModuleId)
                    .FirstOrDefault().CourseId == courseId);

                var testMark = marksInCourseByStudent.Where(y => y.TestMark.HasValue && y.TestMark.Value > 0).Sum(y => y.TestMark.Value);
                var labMark = marksInCourseByStudent.Where(y => y.LabMark.HasValue && y.LabMark.Value > 0).Sum(y => y.LabMark.Value);

                marks.Add(new CourseModuleStudentsMark
                {
                    LabMark = labMark,
                    TestMark = testMark,
                    StudentId = student.StudentId.Value,
                    StudentName = student.Student.User.LastName + " " + student.Student.User.FirstName + " " + student.Student.User.MiddleName
                });
            }

            var modules = await _coursesRepository.GetModulesByCourseIdAsync(course.CourseId);


            int minLabSum = modules.Where(y => y.HasLab).Sum(y => y.MinLabMark.Value);
            int maxLabSum = modules.Where(y => y.HasLab).Sum(y => y.MaxLabMark.Value);
            int minTestSum = modules.Where(y => y.HasTest).Sum(y => y.MinTestMark.Value);
            int maxTestSum = modules.Where(y => y.HasTest).Sum(y => y.MaxTestMark.Value);

            return new CourseRatingViewModel
            {
                Course = course,
                CourseId = courseId,
                Marks = marks,
                MinLabSum = minLabSum,
                MaxLabSum = maxLabSum,
                MinTestSum = minTestSum,
                MaxTestSum = maxTestSum,
            };
        }

        public async Task<ModuleRatingViewModel> GetModuleRating(int courseId, int moduleId)
        {
            var course = await _coursesRepository.GetByIdAsync(courseId);

            var courseStudents = await _marksRepository.GetCourseStudentsByCourseIdAsync(courseId);

            var marks = new List<CourseModuleStudentsMark>();

            var courseModule = (await _coursesRepository.GetCourseModulesAsync()).FirstOrDefault(x => x.ModuleId == moduleId);

            foreach (var student in courseStudents)
            {
                var courseMod = student.Student.Mark;
                var marksInCourseByStudent = student.Student.Mark
                    .Where(y => y.StudentId == student.StudentId && y.CourseModuleId == courseModule.CourseModuleId);

                var testMark = marksInCourseByStudent.Where(y => y.TestMark.HasValue && y.TestMark.Value > 0).Sum(y => y.TestMark.Value);
                var labMark = marksInCourseByStudent.Where(y => y.LabMark.HasValue && y.LabMark.Value > 0).Sum(y => y.LabMark.Value);

                marks.Add(new CourseModuleStudentsMark
                {
                    LabMark = labMark,
                    TestMark = testMark,
                    StudentId = student.StudentId.Value,
                    StudentName = student.Student.User.LastName + " " + student.Student.User.FirstName + " " + student.Student.User.MiddleName
                });
            }

            var module = await _modulesRepository.GetById(moduleId);

            // var modules = await _coursesRepository.GetModulesByCourseIdAsync(course.CourseId);


            int minLabSum = module.MinLabMark ?? 0;
            int maxLabSum = module.MaxLabMark ?? 0;
            int minTestSum = module.MinTestMark ?? 0;
            int maxTestSum = module.MaxTestMark ?? 0;

            return new ModuleRatingViewModel
            {
                Course = course,
                Module = module,
                Marks = marks,
                MinLabSum = minLabSum,
                MaxLabSum = maxLabSum,
                MinTestSum = minTestSum,
                MaxTestSum = maxTestSum,
            };
        }
    }
}
