﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace EducationPlatform.TagHelpers
{
    [HtmlTargetElement("Gravatar")]
    public class GravatarTagHelper : TagHelper
    {
        public string Email { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            byte[] photo = null;
            if (CheckIsConnected())
            {
                photo = GetPhoto(Email);
            }
            else
            {
                photo = File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "user-default.png"));
            }

            string base64String = Convert.ToBase64String(photo);
            output.TagName = "img";
            output.Attributes.SetAttribute("src", $"data:image/jspeg;base64, {base64String}");
        }

        private byte[] GetPhoto(string email)
        {
            var httpClient = new HttpClient();
            return httpClient.GetByteArrayAsync(new Uri($"https://www.gravatar.com/avatar/{HashEmailForGravatar(email)}?d=identicon")).Result;
        }

        public bool CheckIsConnected()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var gravatarResponse = httpClient.GetAsync("https://www.gravatar.com/avatar/").Result;
                    return (gravatarResponse.IsSuccessStatusCode);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private string HashEmailForGravatar(string email)
        {
            var md5Hasher = MD5.Create();
            //.ComputeHash() - конвертує вхідні дані до byte[] і хешує їх 
            byte[] data = md5Hasher.ComputeHash(Encoding.ASCII.GetBytes(email.ToLower()));
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                //.ToString("x2") - форматує строку до шістнадцяткового символу
                stringBuilder.Append(data[i].ToString("x2"));
            }
            return stringBuilder.ToString();
        }

    }
}
