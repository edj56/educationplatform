﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace EducationPlatform.ValidationAttributes
{
    public class AuthorizeBotAttribute : ActionFilterAttribute
    {
        private readonly string apiKeyName = "ApiKey";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            IConfiguration configuration = (IConfiguration)context.HttpContext.RequestServices.GetService(typeof(IConfiguration));

            if (context.HttpContext.Request.Headers[apiKeyName].ToString() != (configuration.GetSection("Bot")).GetValue<string>(apiKeyName))
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}
