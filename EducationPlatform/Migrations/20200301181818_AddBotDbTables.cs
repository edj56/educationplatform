﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EducationPlatform.Migrations
{
    public partial class AddBotDbTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BotAuthorizationKeys",
                columns: table => new
                {
                    KeyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(maxLength: 450, nullable: true),
                    AuthorizationKey = table.Column<string>(unicode: false, maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotAuthorizationKeys_KeyId", x => x.KeyId);
                    table.ForeignKey(
                        name: "FK_BotAuthorizationKeys_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BotAuthorizedUsers",
                columns: table => new
                {
                    AuthorizedUserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(maxLength: 450, nullable: true),
                    ChannelId = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ChannelUserId = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotAuthorizedUsers_KeyId", x => x.AuthorizedUserId);
                    table.ForeignKey(
                        name: "FK_BotAuthorizedUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BotAuthorizationKeys_UserId",
                table: "BotAuthorizationKeys",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BotAuthorizedUsers_UserId",
                table: "BotAuthorizedUsers",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BotAuthorizationKeys");

            migrationBuilder.DropTable(
                name: "BotAuthorizedUsers");
        }
    }
}
