﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EducationPlatform.Migrations
{
    public partial class ChangeNameMisspelling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessangersBots");

            migrationBuilder.CreateTable(
                name: "MessengersBots",
                columns: table => new
                {
                    MessengersBotsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BotId = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    ChannelId = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    BotUrl = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    ServiceUrl = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessengersBots", x => x.MessengersBotsId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessengersBots");

            migrationBuilder.CreateTable(
                name: "MessangersBots",
                columns: table => new
                {
                    MessangersBotsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BotId = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    BotUrl = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    ChannelId = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    ServiceUrl = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessangersBots", x => x.MessangersBotsId);
                });
        }
    }
}
