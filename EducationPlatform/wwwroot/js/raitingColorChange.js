﻿const numberElement = document.getElementById('raitingPercent');
document.addEventListener('DOMContentLoaded', colorNumberElement);

async function getColors() {
    const response = await fetch('/json/color-settings.json');
    return await response.json();
}

async function colorNumberElement() {
    const numberValue = parseInt(numberElement.textContent);
    const colors = await getColors();
    const colorRangeItem = colors.find(color => numberValue >= color.min && numberValue <= color.max);
    numberElement.className = colorRangeItem.color;
}