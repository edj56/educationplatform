const calendar = new FullCalendar.Calendar(document.getElementById('calendar'), {
    plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
    locale: 'uk',
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    defaultDate: (new Date).getTime(),
    editable: false,
    navLinks: true,
    eventLimit: true,
    allDaySlot: false,
    slotEventOverlap: false,
    eventSourceSuccess: () => {
        $('#mySchedule').attr("disabled", false);
        $('#commonSchedule').attr("disabled", false);

        if ($('#mySchedule').is(":hidden")) {
            $('#myScheduleLabel').show();
            $('#commonScheduleLabel').hide();
        } if ($('#commonSchedule').is(":hidden")) {
            $('#commonScheduleLabel').show();
            $('#myScheduleLabel').hide();
        }
    }
});

calendar.render();

$('.fc-button.fc-button-primary')
    .removeClass('fc-button')
    .removeClass('fc-button-primary')
    .addClass('btn btn-inverse-primary btn-sm mx-1');

$('.fc-toolbar.fc-header-toolbar')
    .attr('data-step', 1)
    .attr('data-intro', '')
    .attr('data-position', 'left');

$('.fc-view-container')
    .attr('data-step', 2)
    .attr('data-intro', '')
    .attr('data-position', 'left');

let prevSource;

function getSchedule(url) { 
    $('#mySchedule').attr("disabled", true);
    $('#commonSchedule').attr("disabled", true);
    const source = calendar.addEventSource(url);
    if (prevSource) {
        prevSource.remove();
    }
    prevSource = source;

}

