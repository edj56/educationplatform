﻿$('.timepicker').datetimepicker({
    timepicker: true,
    datepicker: false,
    value: '00:00',
    format: 'H:i',
});

$('.limittimepicker').datetimepicker({
    timepicker: true,
    datepicker: false,
    minTime: '07:00',
    maxTime: '23:59',
    format: 'H:i',
});
