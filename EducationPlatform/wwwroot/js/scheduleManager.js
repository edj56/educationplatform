﻿getSchedule('/Schedule/GetAllCoursesSchedule/');

$('#commonSchedule').click(function () {
    $('#commonSchedule').hide();
    $('#mySchedule').show();
    getSchedule('/Schedule/GetAllCoursesSchedule/');
});


$('#mySchedule').click(function () {
    $('#mySchedule').hide();
    $('#commonSchedule').show();
    getSchedule(url);
});


$("select#courses").change(function () {
    const selectedCourse = $(this).val();
    $.ajax({
        url: `/Schedule/GetCoursesScheduleByFilter/`,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: { coursesId: selectedCourse },
        traditional: true,
        success: function () {
            getSchedule(this.url);
        }
    });
});


$("select#teachers").change(function () {
    const selectedCourse = $(this).val();
    $.ajax({
        url: `/Schedule/GetCoursesScheduleByTeacheFilter/`,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: { emails: selectedCourse },
        traditional: true,
        success: function () {
            getSchedule(this.url);
        }
    });
});