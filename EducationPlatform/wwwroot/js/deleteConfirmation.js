﻿$('.delete-confirmation').submit(function (event) {
    event.preventDefault();
    Swal.fire({
        title: 'Підтвердіть дію',
        text: "Ви впевнені?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#F8F9FA',
        cancelButtonText: '<span style="color:#000">Скасувати</span>',
        confirmButtonText: 'Так'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: event.target.action,
                method: 'POST',
                data: $(event.target).serialize(),
                success: function (response) {
                    if (response.error) {
                        return Swal.fire(
                            'Помилка',
                            response.message,
                            'error'
                        );
                    }
                    if (response.url) {
                        window.location.href = response.url;
                    }
                }
            });
        }
    })
});
