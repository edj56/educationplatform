﻿$("#eyeContainer").on('click', function (event) {
    event.preventDefault();
    if ($('#passwordInput input').attr("type") == "text") {
        $('#passwordInput input').attr('type', 'password');
        $('#eyeContainer i').addClass("fa-eye-slash").removeClass("fa-eye").prop('title', "Показати пароль");
    } else if ($('#passwordInput input').attr("type") == "password") {
        $('#passwordInput input').attr('type', 'text');
        $('#eyeContainer i').removeClass("fa-eye-slash").addClass("fa-eye").prop('title', "Приховати пароль");
    }
});