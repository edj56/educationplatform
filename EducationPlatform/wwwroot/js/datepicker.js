﻿$('.datepicker').datetimepicker({
    timepicker: false,
    datepicker: true,
    format: 'd.m.Y',
    weeks: true
});
