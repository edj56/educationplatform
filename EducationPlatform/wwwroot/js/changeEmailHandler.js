﻿$('#changeEmail').submit(function (event) {
    event.preventDefault();
    Swal.fire({
        title: 'Ви дійсно впевнені?',
        text: "Ваша нова пошта " + $('#inputEmail').val() + "?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#F8F9FA',
        cancelButtonText: '<span style="color:#000">Скасувати<span>',
        confirmButtonText: 'Так, змінити'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: event.target.action,
                method: 'POST',
                data: $(event.target).serialize(),
                success: function (response) {
                    if (response.error && response.message) {
                        return Swal.fire(
                            'Помилка',
                            response.message,
                            'error'
                        );
                    }
                    if (response.url) {
                        Swal.fire(
                            'Успіх',
                            'Email успішно змінений',
                            'success'
                        ).then(() => {
                            window.location.href = response.url;
                        });
                    }
                }
            });
        }
    })
});

function clearEmail() {
    $('#emailModal #inputEmail').val('');
}

$('#emailModal').on('show.bs.modal', clearEmail);
$('#emailModal').on('hide.bs.modal', clearEmail);