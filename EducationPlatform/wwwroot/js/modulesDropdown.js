﻿$('#Course').change(function () {
    var selectedCourse = $("#Course").val();
    var courseModuleSelect = $("#CourseModule");
    courseModuleSelect.empty();
    if (selectedCourse != null && courseModuleSelect.length > 0) {
        $.getJSON(GENERAL.courseJson, { id: selectedCourse },function (courseModules) {
            if (courseModules != null && !jQuery.isEmptyObject(courseModules)) {
                $.each(courseModules, function (index, courseModule) {
                    courseModuleSelect.append(new Option(courseModule.text, courseModule.value));
                });

                $('CourseModule').prop('selectedIndex', 0)
            };
        });
    }
});
