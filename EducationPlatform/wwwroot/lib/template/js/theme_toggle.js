 let themeToggle = document.getElementById("theme-checkbox");
 let body = document.getElementsByTagName("body")[0];
 let whiteLogo = document.getElementById("logo");
 let whiteLogoMini = document.getElementById("logoMini");

window.addEventListener('DOMContentLoaded', function(){
    if(localStorage.getItem('onChecked') === 'true'){
        themeToggle.checked = true;
        if(themeToggle.checked === true){
            ChangeMode();
        } else if(themeToggle.checked === false){
            discardChanges();
        }
    } else if(localStorage.getItem('onChecked') === 'false'){
        themeToggle.checked = false;
    }
    themeToggle.addEventListener("click", ChangeMode);
});

function ChangeMode() {
    if(themeToggle.checked === true){
        localStorage.setItem('onChecked', 'true');
        body.classList.add("dark-theme");
        whiteLogo.src = "/lib/template/images/Logo EP-White.svg";
        whiteLogoMini.src = "/lib/template/images/logo-mini-EP.svg";
    } else{
        localStorage.setItem('onChecked', 'false');
        discardChanges();   
    }
}
function discardChanges(){
    if(themeToggle.checked == false){
        body.classList.remove("dark-theme");
        whiteLogo.src = "/lib/template/images/Logo EP.svg";
        whiteLogoMini.src = "/lib/template/images/logo-mini-EP-black.svg";
    }
}

